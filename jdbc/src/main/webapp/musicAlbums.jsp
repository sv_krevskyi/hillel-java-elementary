<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 07.07.2019
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Albums Collection</title>
</head>
<body>

<table>
    <tr>
        <th>ID</th>
        <th>Artist</th>
        <th>Album name</th>
        <th>Release year</th>
        <th>Genre</th>
    </tr>
    <c:forEach items="${set}" var="album">
        <tr>
            <td>${album.id}</td>
            <td>${album.artist}</td>
            <td>${album.albumName}</td>
            <td>${album.releaseYear}</td>
            <td>${album.genre}</td>
        </tr>
    </c:forEach>
</table>

</body>
</html>