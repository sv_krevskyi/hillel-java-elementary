import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertExample {
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/test?useSSL=false&useUnicode=true&serverTimezone=UTC";

    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);
            System.out.println("Connecting...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected");

            stmt = conn.createStatement();

            String sql = "INSERT INTO MUSIC_ALBUMS (album_name, RELEASE_YEAR, ARTIST, GENRE) VALUES ('Curtis', 2015, 'Curtis Mayfield', 'soul');";

            stmt.executeUpdate(sql);

            System.out.println("Inserted successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
