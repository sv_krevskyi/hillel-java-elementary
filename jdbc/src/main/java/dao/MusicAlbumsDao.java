package dao;

import entity.MusicAlbum;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class MusicAlbumsDao {

    private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private final String DB_URL = "jdbc:mysql://localhost:3306/test?useSSL=false&useUnicode=true&serverTimezone=UTC";

    private final String USER = "root";
    private final String PASS = "root";

    private Connection connection;

    private final String insertSql = "INSERT INTO MUSIC_ALBUMS (album_name, RELEASE_YEAR, ARTIST, GENRE) VALUES (?, ?, ?, ?);";
    private final String selectAlbumSql = "SELECT * FROM music_albums WHERE album_name = ? AND RELEASE_YEAR = ? AND ARTIST = ? AND GENRE = ?;";
    private final String selectByArtistSql = "SELECT * FROM music_albums WHERE ARTIST = ?;";
    private final String selectByYearSql = "SELECT * FROM music_albums WHERE RELEASE_YEAR = ?;";
    private final String deleteById = "DELETE FROM music_albums WHERE id = ?;";

    public MusicAlbumsDao() {
        try {
            Class.forName(JDBC_DRIVER);
            this.connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertNewMusicAlbum(MusicAlbum musicAlbum) throws SQLException {
        try (PreparedStatement stmt = this.connection.prepareStatement(insertSql)) {
            stmt.setString(1, musicAlbum.getAlbumName());
            stmt.setInt(2, musicAlbum.getReleaseYear());
            stmt.setString(3, musicAlbum.getArtist());
            stmt.setString(4, musicAlbum.getGenre());

            stmt.executeUpdate();
        }
    }

    public MusicAlbum selectMusicAlbum(MusicAlbum musicAlbum) throws SQLException {
        MusicAlbum result = null;
        try (PreparedStatement stmt = this.connection.prepareStatement(selectAlbumSql)) {
            stmt.setString(1, musicAlbum.getAlbumName());
            stmt.setInt(2, musicAlbum.getReleaseYear());
            stmt.setString(3, musicAlbum.getArtist());
            stmt.setString(4, musicAlbum.getGenre());

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = createMusicAlbumFromResultSet(rs);
            }
        }
        return result;
    }

    public boolean isMusicAlbumExist(MusicAlbum musicAlbum) throws SQLException {
        boolean result = false;
        if (selectMusicAlbum(musicAlbum) != null) {
            result = true;
        }
        return result;
    }

    public Set<MusicAlbum> selectAllMusicAlbums() throws SQLException {
        Set<MusicAlbum> result = new HashSet<>();
        Statement statement = this.connection.createStatement();
        String sql = "SELECT * FROM MUSIC_ALBUMS;";
        ResultSet rs = statement.executeQuery(sql);
        //MusicAlbum album = createMusicAlbumFromResultSet(rs);
        while (rs.next()) {
            MusicAlbum album = createMusicAlbumFromResultSet(rs);
            result.add(album);
        }
        return result;
    }

    public Set<MusicAlbum> selectAllByArtist(String artist) throws SQLException {
        Set<MusicAlbum> result = new HashSet<>();
        try (PreparedStatement stmt = this.connection.prepareStatement(selectByArtistSql)) {
            stmt.setString(1, artist);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                MusicAlbum album = createMusicAlbumFromResultSet(rs);
                result.add(album);
            }
            return result;
        }
    }

    public Set<MusicAlbum> selectAllByYear(int year) throws SQLException {
        Set<MusicAlbum> result = new HashSet<>();
        try (PreparedStatement stmt = this.connection.prepareStatement(selectByYearSql)) {
            stmt.setString(1, String.valueOf(year));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                MusicAlbum album = createMusicAlbumFromResultSet(rs);
                result.add(album);
            }
            return result;
        }
    }

    private MusicAlbum createMusicAlbumFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("ID");
        int releaseYear = rs.getInt("RELEASE_YEAR");
        String albumName = rs.getString("album_name");
        String artist = rs.getString("artist");
        String genre = rs.getString("genre");

        MusicAlbum result = MusicAlbum.builder()
                .id(id)
                .releaseYear(releaseYear)
                .albumName(albumName)
                .artist(artist)
                .genre(genre).build();
        return result;
    }

    public int deleteMusicAlbum(int id) throws SQLException {
        int result = 0;
        try (PreparedStatement stmt = this.connection.prepareStatement(deleteById)) {
            stmt.setInt(1, id);
            result = stmt.executeUpdate();
            return result;
        }
    }
}