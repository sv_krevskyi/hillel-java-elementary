package dao;

import entity.MusicAlbum;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        try {
            MusicAlbumsDao musicAlbumsDao = new MusicAlbumsDao();
//            MusicAlbum musicAlbum = MusicAlbum.builder()
//                    .albumName("Hz")
//                    .artist("HzArtist")
//                    .genre("vashcheHz")
//                    .releaseYear(2019).build();
//            musicAlbumsDao.insertNewMusicAlbum(musicAlbum);
            Set<MusicAlbum> albums = musicAlbumsDao.selectAllMusicAlbums();
            for (MusicAlbum album : albums) {
                System.out.println(album);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
