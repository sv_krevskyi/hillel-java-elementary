package entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
public class MusicAlbum {
    private int id;
    private String artist;
    private String albumName;
    private int releaseYear;
    private String genre;

    public MusicAlbum(String artist, String albumName, int releaseYear, String genre) {
        this.artist = artist;
        this.albumName = albumName;
        this.releaseYear = releaseYear;
        this.genre = genre;
    }
}
