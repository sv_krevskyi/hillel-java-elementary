package util;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class JsonUtil {

    private static Gson gson = new Gson();

    public static void replyWithStatusCode(HttpServletResponse resp, Object response, int statusCode) throws IOException {
        String jsonResponse = gson.toJson(response);
        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.setStatus(statusCode);
        out.print(jsonResponse);
        out.flush();
    }

    public static <T> T buildObjectFromJson(HttpServletRequest req, Class<T> clazz) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = req.getReader();
        String str;

        while ((str = br.readLine()) != null) {
            sb.append(str);
        }

        T object = gson.fromJson(sb.toString(), clazz);
        return object;
    }
}
