package programming;

public class AddTwoNumbers {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode root;
        int sum = l1.val + l2.val;
        if (sum < 10) {
            root = new ListNode(sum);
        } else {
            root = new ListNode(sum % 10);
            if (l1.next != null)
                l1.next.val++;
            else
                root.next = new ListNode(1);
        }
        while (l1.next != null){
            root.next = addTwoNumbers(l1.next, l2.next);
        }
        return root;
    }

    private class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }
}


