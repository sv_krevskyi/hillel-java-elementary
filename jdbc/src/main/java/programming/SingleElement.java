package programming;

public class SingleElement {
    public static int findMissingElement(int[] arr) {
        int sum = 0;
        int expectedSum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if (i % 2 == 0){
                expectedSum+= 2*arr[i];
            }
        }
        return expectedSum - sum;
    }
}
