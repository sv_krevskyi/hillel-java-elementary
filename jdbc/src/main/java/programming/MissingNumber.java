package programming;

public class MissingNumber {

    public static int findMissingNumber(int[] arr) {
        int sum = 0;
        int expectedSum = 0;
        int index = 0;
        for (int num : arr) {
            sum += num;
            expectedSum += ++index;
        }
        return expectedSum - sum;
    }

    static int getMissingNo(int a[], int n)//from GeeksForGeeks
    {
        int i, total;
        total = (n + 1) * (n + 2) / 2;
        for (i = 0; i < n; i++)
            total -= a[i];
        return total;
    }
}
