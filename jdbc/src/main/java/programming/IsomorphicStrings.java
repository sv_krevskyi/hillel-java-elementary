package programming;

import java.util.HashMap;
import java.util.Map;

public class IsomorphicStrings {
    public static boolean check(String first, String second) {
        boolean result = true;
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < first.length(); i++) {
            String key = first.substring(i, i + 1);
            String value = second.substring(i, i + 1);
            boolean condition1 = map.containsKey(key) && !map.get(key).equals(value);
            boolean condition2 = map.containsValue(value) && !map.containsKey(key);
            if (condition1 || condition2) {
                result = false;
                break;
            }

            map.put(key, value);
        }
        return result;
    }
}
