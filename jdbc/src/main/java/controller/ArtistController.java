package controller;

import entity.MusicAlbum;
import entity.Response;
import service.MusicAlbumService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static util.JsonUtil.replyWithStatusCode;

@WebServlet(urlPatterns = "/music_albums/artists/*")
public class ArtistController extends HttpServlet {

    MusicAlbumService musicAlbumService = new MusicAlbumService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] pathInfo = req.getPathInfo().split("/");
        String artistName = pathInfo[1];
        Set<MusicAlbum> albums = null;
        Response response;
        try {
            albums = musicAlbumService.getAllByArtist(artistName);
            if (albums.isEmpty()) {
                response = new Response("There is no such artist(" + artistName + ") in database.");
                replyWithStatusCode(resp, response, 404);
            } else {
                replyWithStatusCode(resp, albums, 200);
            }
        } catch (SQLException e) {
            response = new Response("Error occurred: " + e.getMessage());
            replyWithStatusCode(resp, response, 500);
            e.printStackTrace();
        }
    }

}
