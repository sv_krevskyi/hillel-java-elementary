package controller;

import entity.MusicAlbum;
import entity.Response;
import exception.RecordIsAlreadyInDatabaseException;
import service.MusicAlbumService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static util.JsonUtil.buildObjectFromJson;
import static util.JsonUtil.replyWithStatusCode;

@WebServlet(urlPatterns = ("/music_albums"))
public class MusicAlbumController extends HttpServlet {

    private MusicAlbumService musicAlbumService = new MusicAlbumService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Set<MusicAlbum> allMusicAlbums = null;
        try {
            allMusicAlbums = musicAlbumService.getAllMusicAlbums();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        replyWithStatusCode(resp, allMusicAlbums, 200);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        MusicAlbum musicAlbum = buildObjectFromJson(req, MusicAlbum.class);
        Response response;
        try {
            if (musicAlbumService.isMusicAlbumExist(musicAlbum)) {
                throw new RecordIsAlreadyInDatabaseException();
            }
            musicAlbumService.createNewMusicAlbum(musicAlbum);
            response = new Response("Music album " + musicAlbum.toString() + " created successfully");
            replyWithStatusCode(resp, response, 201);
        } catch (SQLException e) {
            response = new Response("Error occurred: " + e.getMessage());
            replyWithStatusCode(resp, response, 500);
            e.printStackTrace();
        } catch (RecordIsAlreadyInDatabaseException e) {
            response = new Response("Cannot create music album " + musicAlbum.toString() + ". Already exists");
            replyWithStatusCode(resp, response, 409);
            e.printStackTrace();
        }
    }
}
