package controller;

import entity.MusicAlbum;
import entity.Response;
import service.MusicAlbumService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import static util.JsonUtil.replyWithStatusCode;

@WebServlet(urlPatterns = "/music_albums/year/*")
public class YearController extends HttpServlet {

    MusicAlbumService musicAlbumService = new MusicAlbumService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();
        String[] pathComponents = path.split("/");
        int year = Integer.parseInt(pathComponents[1]);
        Set<MusicAlbum> albums = new HashSet<>();
        Response response;
        try {
            albums = musicAlbumService.getAllByYear(year);
            if (albums.isEmpty()) {
                response = new Response("There are no albums released in " + year + " available in database.");
                replyWithStatusCode(resp, response, 404);
            } else {
                replyWithStatusCode(resp, albums, 200);
            }
        } catch (SQLException e) {
            response = new Response("Error occurred: " + e.getMessage());
            replyWithStatusCode(resp, response, 500);
            e.printStackTrace();
        }
    }

}
