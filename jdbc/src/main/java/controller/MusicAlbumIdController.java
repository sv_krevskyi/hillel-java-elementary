package controller;

import entity.Response;
import service.MusicAlbumService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static util.JsonUtil.replyWithStatusCode;

@WebServlet(urlPatterns = "/music_albums/id/*")
public class MusicAlbumIdController extends HttpServlet {
    MusicAlbumService musicAlbumService = new MusicAlbumService();

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] pathInfo = req.getPathInfo().split("/");
        int id = Integer.parseInt(pathInfo[1]);
        Response response;
        try {
            if (musicAlbumService.deleteMusicAlbum(id) == 0) {
                response = new Response("There is no Music Albums in DB with such ID - " + id);
                replyWithStatusCode(resp, response, 404);
            } else {
                response = new Response("Music album" + "successfully deleted.");
                replyWithStatusCode(resp, response, 200);
            }
        } catch (SQLException e) {
            response = new Response("Error occurred: " + e.getMessage());
            replyWithStatusCode(resp, response, 500);
            e.printStackTrace();
        }
    }
}
