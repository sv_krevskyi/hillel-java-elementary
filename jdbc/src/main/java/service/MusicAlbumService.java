package service;

import dao.MusicAlbumsDao;
import entity.MusicAlbum;

import java.sql.SQLException;
import java.util.Set;

public class MusicAlbumService {
    private final MusicAlbumsDao musicAlbumsDao = new MusicAlbumsDao();

    public Set<MusicAlbum> getAllMusicAlbums() throws SQLException {
        return musicAlbumsDao.selectAllMusicAlbums();
    }

    public void createNewMusicAlbum(MusicAlbum musicAlbum) throws SQLException {
        musicAlbumsDao.insertNewMusicAlbum(musicAlbum);
    }

    public boolean isMusicAlbumExist(MusicAlbum musicAlbum) throws SQLException {
        return musicAlbumsDao.isMusicAlbumExist(musicAlbum);
    }

    public Set<MusicAlbum> getAllByArtist(String artist) throws SQLException {
        return musicAlbumsDao.selectAllByArtist(artist);
    }

    public Set<MusicAlbum> getAllByYear(int year) throws SQLException {
        return musicAlbumsDao.selectAllByYear(year);
    }

    public int deleteMusicAlbum(int id) throws SQLException {
        return musicAlbumsDao.deleteMusicAlbum(id);
    }
}
