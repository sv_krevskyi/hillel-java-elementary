import java.sql.*;

public class SelectExample {
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/test?useSSL=false&useUnicode=true&serverTimezone=UTC";

    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);
            System.out.println("Connecting...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected");

            stmt = conn.createStatement();

            String sql = "SELECT * FROM MUSIC_ALBUMS;";

            ResultSet rs = stmt.executeQuery(sql);
            System.out.println(rs);

            while (rs.next()){
                int id = rs.getInt("ID");
                int releaseYear = rs.getInt("RELEASE_YEAR");
                String albumName = rs.getString("album_name");
                String artist = rs.getString("artist");
                String genre = rs.getString("genre");

                System.out.println(id + " " + releaseYear + " " + albumName + " " + artist + " " + genre);
            }

            System.out.println("Selected successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
