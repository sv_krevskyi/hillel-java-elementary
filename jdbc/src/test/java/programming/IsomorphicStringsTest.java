package programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class IsomorphicStringsTest {

    @Test
    public void shouldReturnTrueForIsomorphicStrings() {
        Assertions.assertTrue(IsomorphicStrings.check("paper", "title"));
    }

    @Test
    public void shouldReturnFalseForNotIsomorphicStrings() {
        Assertions.assertFalse(IsomorphicStrings.check("aa", "ab"));
    }
}
