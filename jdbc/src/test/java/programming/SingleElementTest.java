package programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SingleElementTest {

    @Test
    public void shouldReturnMissingSingleElementFromIntArray() {

        int arr[] = {3,3,7,7,10,10,11};
        int actual = SingleElement.findMissingElement(arr);
        Assertions.assertEquals(11, actual);
    }
}
