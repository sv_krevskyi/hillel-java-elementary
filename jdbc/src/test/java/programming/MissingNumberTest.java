package programming;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MissingNumberTest {

    @Test
    public void shouldReturnMissingNumberInIntArray() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//        int[] arr = {3, 0, 1};
//        int[] arr = {4, 0, 2, 3};
        int actual = MissingNumber.findMissingNumber(arr);
        Assertions.assertEquals(0, actual);
    }
}
