package EnterpriseTest;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class IncrementSyncronize {
    private int value = 0;
    private Semaphore semaphore = new Semaphore(1, true);
    private AtomicInteger atomicValue = new AtomicInteger(value);

    public synchronized int getNextValueSyncronized() {
        return ++value;
    }

    public int getNextValueSyncronizedBlock() {
        int result;
        synchronized (this) {
            result = ++value;
        }
        return result;
    }

    public int getNextValueSemaphore() {
        int result = value;
        try {
            semaphore.acquire();
            result = ++value;
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int getNextValueAtomicInt() {
        return atomicValue.incrementAndGet();
    }
}
