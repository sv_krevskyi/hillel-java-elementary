package EnterpriseTest;

public class MainThread {
    public static void main(String[] args) {
        IncrementSyncronize increment = new IncrementSyncronize();

        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(() -> System.out.println(Thread.currentThread().getName() +
                    "\n" + increment.getNextValueSemaphore()));
            thread.start();
        }
    }
}
