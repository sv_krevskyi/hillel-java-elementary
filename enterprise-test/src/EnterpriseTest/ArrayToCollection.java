package EnterpriseTest;

import java.util.Collection;
import java.util.HashSet;

public class ArrayToCollection {
    public static void main(String[] args) {
        String[] strings = {"1", "2", "3", "4", "5"};
        HashSet<String> col = new HashSet<>();
        Transfer(strings, col);
        for (String item : col) {
            System.out.println(item);
        }
    }

    private static <T> void Transfer(T[] array, Collection<T> collection) {
        for (T item : array) {
            collection.add(item);
        }
    }
}
