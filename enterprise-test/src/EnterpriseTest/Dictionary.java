package EnterpriseTest;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;

public class Dictionary {
    public static void main(String[] args) {
        String filePath = "C:\\Users\\user\\Desktop\\javaEEtest\\src\\EnterpriseTest\\randomtext.txt";
        HashSet<String> dictionary = getDistinctWordsFromFile(filePath);
        for (String word : dictionary) {
            System.out.println(word);
        }
    }

    private static HashSet<String> getDistinctWordsFromFile(String filePath) {
        HashSet<String> result = new HashSet<>();
        try (Scanner scanner = new Scanner(new File(filePath))) {
            while (scanner.hasNextLine()) {
                Scanner lineScanner = new Scanner(scanner.nextLine());
                lineScanner.useDelimiter(" ");
                while (lineScanner.hasNext()) {
                    String rawWord = lineScanner.next();
                    String word = rawWord.replaceAll("[.,/\\-;:]", "").toLowerCase();
                    result.add(word);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }
}
