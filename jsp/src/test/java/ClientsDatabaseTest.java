import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.util.HashSet;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ClientsDatabaseTest {

    private ClientsDatabase clientsDatabase;
    private HashSet<Client> set;
    private Client clientTestExample;

    @BeforeAll
    void initDatabaseAndClientExamples() {
        initDatabase();
        initClientExample();

        Assertions.assertFalse(set.contains(clientTestExample));
    }

    @Test//PositiveTest
    public void checkIfAddToDatabaseMethodActuallyAddsClientToDatabase() {
        try {
            clientsDatabase.addToDatabase(clientTestExample);
        } catch (ClientExistsException e) {
            e.printStackTrace();
        }
        Assertions.assertTrue(set.contains(clientTestExample));
    }

    @Test//NegativeTest
    public void ExceptionShouldBeThrownWhenTryingToAddClientThatAlreadyExistInDatabase() {
        set.add(clientTestExample);
        Assertions.assertThrows(ClientExistsException.class, () -> clientsDatabase.addToDatabase(clientTestExample));
    }

    @AfterEach
    void removeClientTestExampleFromDatabase(){
        set.remove(clientTestExample);
    }

    private void initDatabase() {
        clientsDatabase = ClientsDatabase.getInstance();
        try {
            Field field = clientsDatabase.getClass().getDeclaredField("database");
            field.setAccessible(true);//Should I setAccessible(false) after next line?
            set = (HashSet<Client>) field.get(clientsDatabase);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void initClientExample() {
        clientTestExample = new Client("IAm", "AlreadyInDatabase");
    }

}
