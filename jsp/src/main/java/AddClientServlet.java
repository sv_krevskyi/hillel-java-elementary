import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = ("/clients"))
public class AddClientServlet extends HttpServlet {

    ClientsDatabase database = ClientsDatabase.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String first_name = req.getParameter("first_name");
        String last_name = req.getParameter("last_name");

        Client client = new Client(first_name, last_name);

        try {
            database.addToDatabase(client);
        } catch (ClientExistsException e) {
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

        req.getRequestDispatcher("/printmessage.jsp").forward(req, resp);
    }
}
