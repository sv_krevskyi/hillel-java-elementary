import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashSet;

@WebServlet(urlPatterns = ("/clients_table"))
public class ClientsTable extends HttpServlet {

    ClientsDatabase database = ClientsDatabase.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("set", getHashSet());
        req.getRequestDispatcher("/clientsTable.jsp").forward(req, resp);
    }

    private HashSet<Client> getHashSet() {
        HashSet<Client> set = null;
        try {
            Field field = database.getClass().getDeclaredField("database");
            field.setAccessible(true);//Should I setAccessible(false) after next line?
            set = (HashSet<Client>) field.get(database);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return set;
    }
}
