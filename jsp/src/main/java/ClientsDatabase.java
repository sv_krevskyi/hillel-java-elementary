import java.util.HashSet;
import java.util.Iterator;

public class ClientsDatabase{

    private static ClientsDatabase instance;

    private HashSet<Client> database;

    private ClientsDatabase() {
        database = new HashSet();
    }

    public void addToDatabase(Client client) throws ClientExistsException{
        boolean result = database.add(client);
        if (!result){
            throw new ClientExistsException();
        }
    }

    public static ClientsDatabase getInstance() {
        if (ClientsDatabase.instance != null)
            return ClientsDatabase.instance;
        else {
            ClientsDatabase.instance = new ClientsDatabase();
            return ClientsDatabase.instance;
        }
    }

}
