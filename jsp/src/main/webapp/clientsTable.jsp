<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 07.07.2019
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Clients List</title>
</head>
<body>

<table>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
    </tr>
    <c:forEach items="${set}" var="client">
        <tr>
            <td>${client.firstName}</td>
            <td>${client.lastName}</td>
         </tr>
    </c:forEach>
</table>

<form name="return" action="home.jsp" method='post'>
    <input type='submit' value='Return Home'/>
</form>

</body>
</html>
