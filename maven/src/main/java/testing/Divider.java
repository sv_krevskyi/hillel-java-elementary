package testing;

public class Divider {

    static double divide(int i, int j) {
        if (i == 0 || j == 0) {
            throw new IllegalArgumentException("Argument can not be Zero");
        }
        return i / j;
    }
}
