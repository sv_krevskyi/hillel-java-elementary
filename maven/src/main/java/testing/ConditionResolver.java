package testing;

public class ConditionResolver {

    static boolean isTwoStringsEqualIgnoreCase(String one, String two) {
        return one.equalsIgnoreCase(two);
    }
}
