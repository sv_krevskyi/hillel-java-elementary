package testing;

import lombok.AllArgsConstructor;

import java.io.IOException;
import java.io.Writer;

@AllArgsConstructor
public class FileWriter {
    private String path;

    void writeTextToFile(String text) throws IOException {

        try (Writer writer = new java.io.FileWriter(this.path)) {
            writer.write(text);

        } catch (IOException e) {
            throw e; //attention
        }
    }

}
