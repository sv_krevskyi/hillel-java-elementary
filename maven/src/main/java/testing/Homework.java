package testing;

public class Homework {

    public static boolean isIncreasing(int[] array) {

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }


    public static String reverseString(String str) {
        char[] sourceString = str.toCharArray();
        char temp;
        for (int i = 0; i < sourceString.length / 2; i++) {
            temp = sourceString[i];
            sourceString[i] = sourceString[sourceString.length - 1 - i];
            sourceString[sourceString.length - 1 - i] = temp;
        }
        return new String(sourceString);
    }

}
