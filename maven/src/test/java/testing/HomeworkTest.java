package testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HomeworkTest {

    @Test
    public void returnTrueIfElementsInArrayIsInIncreasingOrder() {
        int[] testArray = {1, 2, 3, 4};
        Assertions.assertTrue(Homework.isIncreasing(testArray));
    }

    @Test
    public void returnFalseIfElementsInArrayAreNotInIncreasingOrder() {
        int[] testArray = {1, 2, 3, 2};
        Assertions.assertFalse(Homework.isIncreasing(testArray));
    }

    @Test
    public void returnFalseIfThereAreTwoEqualConsecutiveElementInArray() {
        int[] testArray = {1, 2, 2, 3};
        Assertions.assertFalse(Homework.isIncreasing(testArray));
    }

    @Test
    public void returnedStringShouldBeReversed() {
        String testedString = "asdfghj";
        String reversedString = Homework.reverseString(testedString);
        Assertions.assertEquals("jhgfdsa", reversedString);
    }

    @Test
    public void emptyStringInputShouldReturnEmptyString() {
        Assertions.assertEquals("", Homework.reverseString(""));
    }
}
