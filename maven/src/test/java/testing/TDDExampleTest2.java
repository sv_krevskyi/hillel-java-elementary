package testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class TDDExampleTest2 {

    @Test
    public void returnCorrectElementOfTheList() {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Assertions.assertEquals(5, TDDExample2.getElementOfList(5, list));
    }
}
