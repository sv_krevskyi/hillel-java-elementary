package testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class TDDExampleTest {

    @Test
    public void listSizeShouldBeReturnedCorrectly () {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        Assertions.assertEquals(5, TDDExample.getSizeOfList(list));
    }


}
