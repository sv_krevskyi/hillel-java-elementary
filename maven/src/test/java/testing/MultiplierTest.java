package testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MultiplierTest {

    @Test
    public void multiplicationShouldBeDoneCorrectlyTest() {
        Assertions.assertEquals(10, Multiplier.multiply(5, 2));
    }

    @Test
    public void multiplicationByZeroShouldBeZero() {
        Assertions.assertEquals(0, Multiplier.multiply(7654,0));
    }
}
