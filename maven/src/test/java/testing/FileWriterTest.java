package testing;

import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FileWriterTest {
    private FileWriter fileWriter;
    private File file;

    @BeforeAll
    void createFile() {
        System.out.println("Before All");
        try {
            file = File.createTempFile( "some-prefix", "some-text");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        fileWriter = new FileWriter(file.getAbsolutePath());
    }

    @BeforeEach
    void cleanFile() throws FileNotFoundException {
        System.out.println("Before Each");
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    private String readContent(File file) throws IOException {
        return new String(Files.readAllBytes(file.toPath()));
    }

    @Test
    void contentShouldBeWrittenCorrectly() throws IOException {
        System.out.println("Test");
        String text = "test text";
        fileWriter.writeTextToFile(text);
        Assertions.assertEquals(text, readContent(file));
    }

    @AfterAll
    void deleteFile() {
        System.out.println("After All");
        file.delete();
    }
}
