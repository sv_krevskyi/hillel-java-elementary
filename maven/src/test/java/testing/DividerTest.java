package testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DividerTest {

    @Test
    public void dividingShouldBeDoneCorrectly() {
        Assertions.assertEquals(10, Divider.divide(100, 10));
    }

    @Test
    public void exceptionShouldBeThrownZeroInArgumentTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Divider.divide(1,0), "Arguments cannot be zero");
    }
}
