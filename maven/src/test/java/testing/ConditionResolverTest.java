package testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConditionResolverTest {

    @Test
    public void twoEqualStringsShouldBeEqual() {
        Assertions.assertTrue(ConditionResolver.isTwoStringsEqualIgnoreCase("one", "one"));
    }

    @Test
    public void twoEqualStringsWithDifferentRegisterShouldBeEqual() {
        Assertions.assertTrue(ConditionResolver.isTwoStringsEqualIgnoreCase("oNe", "OnE"));
    }

    @Test
    public void twoNotEqualStringsShouldBeNotEqual() {
        Assertions.assertFalse(ConditionResolver.isTwoStringsEqualIgnoreCase("one", "two"));
    }

}
