package week11.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Advanced {

    public static void main(String[] args) {
        List<Book> library = new ArrayList<>();

        library.add(new Book("title1", new Author("author1", 22, Gender.FEMALE)));
        library.add(new Book("title2", new Author("author1", 22, Gender.MALE)));
        library.add(new Book("title3", new Author("author7", 51, Gender.FEMALE)));
        library.add(new Book("title4", new Author("author3", 13, Gender.MALE)));
        library.add(new Book("title5", new Author("author2", 124, Gender.FEMALE)));


        Map<Author, List<Book>> authorBookMap = library.stream().collect(Collectors.groupingBy(b -> b.getAuthor()));
        System.out.println(authorBookMap);


        System.out.println(library.stream().map(b -> b.getAuthor().getAge()).reduce(0, (p1, p2) -> p1 + p2));

        System.out.println();
        //1st task - get all female authors
        List<Author> femaleAuthors = library
                .stream()
                .map(book -> book.getAuthor())
                .filter(a -> a.getGender() == Gender.FEMALE)
                .collect(Collectors.toList());
        System.out.println(Arrays.toString(femaleAuthors.toArray()));

        //2nd task - get all female authors under 30 y.o.
        Long numberOfFemaleAuthorsUnder30 = library
                .stream()
                .map(book -> book.getAuthor())
                .filter(a -> a.getGender() == Gender.FEMALE)
                .filter(a -> a.getAge() < 30)
                .collect(Collectors.counting());
        System.out.println("Female authors under 30: " + numberOfFemaleAuthorsUnder30);

        //3rd task - get all male authors over 30 y.o. names
        List<String> maleOver30AuthorsNames = library
                .stream()
                .map(book -> book.getAuthor())
                .filter(a -> a.getGender() == Gender.MALE)
                .filter(a -> a.getAge() > 30)
                .map(a -> a.getSurname().toUpperCase())
                .distinct()
                .collect(Collectors.toList());
        System.out.println(Arrays.toString(maleOver30AuthorsNames.toArray()));


        //GOD LEVEL task - get average age of all male authors
        Double averageMaleAuthorsAge = library
                .stream()
                .map(book -> book.getAuthor())
                .filter(a -> a.getGender() == Gender.MALE)
                .collect(Collectors.averagingInt(Author::getAge));
        System.out.println("Male authors average age: " + averageMaleAuthorsAge);

    }
}
