package week5.abstraction.homework;

public abstract class MovieCrewMember {
    private String firstName;
    private String lastName;

    public MovieCrewMember(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String name){
        this.firstName = name;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName(String name){
        this.lastName = name;
    }

    public abstract void participateInMovie(Movie movie);
}
