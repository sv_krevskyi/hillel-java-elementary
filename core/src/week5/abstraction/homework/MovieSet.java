package week5.abstraction.homework;

public class MovieSet {

    MovieCrewMember[] crewMembers;

    public static void main(String[] args) {

        Script avengersScript = new Script();
        avengersScript.setScriptText("The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe.");
        Movie avengers = new Movie("Avengers", "Comics/Action", avengersScript);

        Director director = new Director("Anthony", "Russo");

        Cameraman cameraman = new Cameraman("Trent", "Opaloch");

        Actor downeyJr = new Actor("Robert", "Downey Jr.");
        downeyJr.setIsMainRole(true);

        Actor evans = new Actor("Chris", "Evans");
        evans.setIsMainRole(true);

        Actor larson = new Actor("Brie", "Larson");
        larson.setIsMainRole(false);

        MovieCrewMember[] movieCrew = addCrewMember(director, cameraman, downeyJr, evans, larson);

        for (MovieCrewMember member : movieCrew) {
            if (member instanceof ScriptKnowledge)
                ((ScriptKnowledge) member).setScript(avengersScript);
            member.participateInMovie(avengers);
        }

    }

    static MovieCrewMember[] addCrewMember(MovieCrewMember... members) {
        return members;
    }

}
