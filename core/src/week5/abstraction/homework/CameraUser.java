package week5.abstraction.homework;

public interface CameraUser {

    void useCamera();
}
