package week5.abstraction.homework;

public class Script {

    private String scriptText;

    public String getScriptText() {
        return scriptText;
    }

    public void setScriptText(String scriptText) {
        this.scriptText = scriptText;
    }
}
