package week5.abstraction.homework;

public class Actor extends  MovieCrewMember  implements  ScriptKnowledge {
    private boolean isMainRole;
    private Script script;

    public Actor(String firstName, String lastName) {
        super(firstName, lastName);
    }


    @Override
    public void participateInMovie(Movie movie) {
        System.out.printf("My name is %s %s and I`m an actor. I participate in %s the movie and its %b that I have main role.\n", getFirstName(), getLastName(), movie.getName(), isMainRole());
    }

    public Script getScript(){
        return script;
    }

    @Override
    public void setScript(Script script) {

    }

    public boolean isMainRole(){
        return isMainRole;
    }

    public void setIsMainRole(boolean value){
        this.isMainRole = value;
    }


}
