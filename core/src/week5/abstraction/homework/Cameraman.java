package week5.abstraction.homework;

public class Cameraman extends  MovieCrewMember implements CameraUser {

    public Cameraman(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void useCamera() {
        System.out.printf("...noise of camera\n");
    }

    @Override
    public void participateInMovie(Movie movie) {
        System.out.printf("My name is %s %s and I`m a cameraman. I participate in %s the movie.\n", getFirstName(), getLastName(), movie.getName());
        useCamera();
    }
}
