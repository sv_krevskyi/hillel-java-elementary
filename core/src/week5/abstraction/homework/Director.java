package week5.abstraction.homework;

public class Director extends  MovieCrewMember implements ScriptKnowledge {
    private Script script;

    public Director(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public Script getScript(){
        return script;
    }
    public void setScript(Script script){
        this.script = script;
    }
    @Override
    public void participateInMovie(Movie movie){
        System.out.printf("My name is %s %s and I`m a director. I participate in %s the movie and i know its script.\n", getFirstName(), getLastName(), movie.getName());
    }
}
