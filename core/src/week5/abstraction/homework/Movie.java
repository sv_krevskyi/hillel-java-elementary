package week5.abstraction.homework;

public class Movie {

    private String name;
    private String genre;
    private Script script;

    public Movie(String name, String genre, Script script){
        this.name = name;
        this.genre = genre;
        this.script = script;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }
}
