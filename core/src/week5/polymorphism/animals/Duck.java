package week5.polymorphism.animals;

public class Duck extends Animal {

    @Override
    public void makeSound() {
        System.out.println("Quack!");
    }
}
