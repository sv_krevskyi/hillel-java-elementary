package week5.polymorphism.animals.factory;

import week5.polymorphism.animals.Animal;
import week5.polymorphism.animals.Dog;
import week5.polymorphism.animals.Duck;
import week5.polymorphism.animals.Cat;

import java.util.Scanner;

public class AnimalFactory {

    public Animal createAnimal(String type){
//       if (type.equalsIgnoreCase("cat"))
//           return new Cat();
//       else if (type.equalsIgnoreCase("duck"))
//           return new Duck();
//       else if (type.equalsIgnoreCase("dog"))
//           return new Dog();
//       else return null;
        Animal animal;
       switch (type.toLowerCase()){
           case "cat":
               animal = new Cat();
               break;
           case "duck":
               animal = new Duck();
               break;
           case "dog":
               animal = new Dog();
               break;
           default:
               animal = null;
               break;
       }
       return animal;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String argument = scanner.nextLine();
        AnimalFactory animalFactory = new AnimalFactory();
        Animal animal = animalFactory.createAnimal(argument);

        if (animal != null) {
            animal.makeSound();
        } else {
            System.out.println("boroda");
        }
    }
}
