package week5.polymorphism.animals;

public class Dog extends Animal {

    @Override
    public void makeSound() {
        System.out.println("Woof!");
    }
}
