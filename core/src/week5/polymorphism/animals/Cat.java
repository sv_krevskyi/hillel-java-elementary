package week5.polymorphism.animals;

public class Cat extends Animal {

    @Override
    public void makeSound() {
        System.out.println("Nya!");
    }
}
