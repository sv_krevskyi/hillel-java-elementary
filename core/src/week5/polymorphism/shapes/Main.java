package week5.polymorphism.shapes;

public class Main {
    public static void main(String[] args) {
        //rectangle test
        Shape rectangle = new Rectangle(5 ,7);
        System.out.println("Rectangle area: " + rectangle.area() + ";\nRectangle perimeter: " + rectangle.perimeter());
        System.out.println();

        //Circle test
        Shape circle = new Circle(5);
        System.out.println("Circle area: " + circle.area() + ";\nCircle perimeter: " + circle.perimeter());
        System.out.println();

        System.out.println();
        //Triangle test
        Shape triangle = new Triangle(5,3,4);
        System.out.println("triangle area: " + triangle.area() + ";\nTriangle perimeter: " + triangle.perimeter());
    }
}
