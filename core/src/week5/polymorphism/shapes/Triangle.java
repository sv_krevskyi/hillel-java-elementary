package week5.polymorphism.shapes;

public class Triangle extends Shape {

    double sideA;
    double sideB;
    double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    @Override
    public double area() {
        double s = 0.5 * perimeter();
        double result = Math.sqrt(s * (s - sideA) * (s - sideB) * (s - sideC));
        return result;
    }

    @Override
    public double perimeter() {
        return sideA + sideB + sideC;
    }
}
