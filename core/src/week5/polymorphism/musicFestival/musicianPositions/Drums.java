package week5.polymorphism.musicFestival.musicianPositions;

import week5.polymorphism.musicFestival.Instrument;

public class Drums extends Instrument {

    public Drums(String name){
        super(name);
    }

    @Override
    public void makeSound() {
        System.out.println("Badumts!");
    }
}
