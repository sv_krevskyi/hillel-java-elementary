package week5.polymorphism.musicFestival.musicianPositions;

import week5.polymorphism.musicFestival.Instrument;

public class Vocal extends Instrument {

    public Vocal(String name){
        super(name);
    }

    @Override
    public void makeSound() {
        System.out.println("Aaaaaaaaaaa");
    }
}
