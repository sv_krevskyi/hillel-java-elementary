package week5.polymorphism.musicFestival.musicianPositions;

import week5.polymorphism.musicFestival.Instrument;

public class Guitar extends Instrument {

    public Guitar(String name){
        super(name);
    }

    @Override
    public void makeSound() {
        System.out.println("wrum-wrum");
    }
}
