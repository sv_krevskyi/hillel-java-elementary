package week5.polymorphism.musicFestival;

import week5.polymorphism.musicFestival.musicianPositions.Drums;
import week5.polymorphism.musicFestival.musicianPositions.Guitar;
import week5.polymorphism.musicFestival.musicianPositions.Vocal;

public class Main {
    public static void main(String[] args) {

        Musician pupkin = new Musician("Pupkin", "vocals", new Vocal("soprano"));
        Musician pupkin2 = new Musician("Pupkin2", "drums", new Drums("yamaha"));
        Musician pupkin3 = new Musician("Pupkin3", "guitar", new Guitar("gibson"));

        MusicBand pupkins = new MusicBand("thePupkins", "popsa", pupkin, pupkin2, pupkin3);


        Festival zahidFest = new Festival("ZahidFest",pupkins);
        zahidFest.beginFestival();

    }
}
