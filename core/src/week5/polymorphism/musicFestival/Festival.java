package week5.polymorphism.musicFestival;

public class Festival {

    String name;
    MusicBand[] musicBand;

    public Festival(String name, MusicBand... musicBand) {
        this.name = name;
        this.musicBand = musicBand;
    }

    public void beginFestival() {
        for (MusicBand band : musicBand) {
            band.playSongs();
        }
    }
}
