package week5.polymorphism.musicFestival;

public class Instrument {
    String name;

    public Instrument(String name) {
        this.name = name;
    }

    public void makeSound() {
        System.out.println("Some sound");
    }
}
