package week5.polymorphism.musicFestival;

public class Musician {
    String name;
    String position;
    Instrument instrument;

    public Musician(String name, String position, Instrument instrument) {
        this.name = name;
        this.position = position;
        this.instrument = instrument;
    }

    public void playMusic() {
        useInstrument();
    }

    void useInstrument() {
        this.instrument.makeSound();
    }
}
