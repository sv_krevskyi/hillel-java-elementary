package week5.polymorphism.musicFestival;

public class MusicBand {
    String name;
    String genre;
    Musician[] musicians;

    public MusicBand(String name, String genre, Musician... musicians) {
        this.name = name;
        this.genre = genre;
        this.musicians = musicians;
    }

    public void playSongs() {
        for (Musician musician : musicians) {
            musician.playMusic();
        }
    }
}
