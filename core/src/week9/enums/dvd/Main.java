package week9.enums.dvd;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DvdController dvdController = new DvdController();

        int value;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number:");

        while ((value = scanner.nextInt()) > 0){
            dvdController.processRequest(value);
            System.out.println("Enter number:");
        }
    }
}
