package week9.enums.dvd;

public enum DvdState {
    ON, STANDBY, OFF;
}
