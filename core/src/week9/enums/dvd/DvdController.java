package week9.enums.dvd;

public class DvdController {
    public DvdController() {
        this.dvdState = DvdState.OFF;
    }

    public DvdState dvdState;

    public void processRequest(int value){
        switch (value){
            case 1:
                dvdState = DvdState.ON;
                break;
            case 2:
                dvdState = DvdState.STANDBY;
                break;
            case 3:
                dvdState = DvdState.OFF;
                break;
        }
    }
}
