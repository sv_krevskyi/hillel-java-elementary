package week9.regex;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Db dbManager = new Db();
        int numberOfUsers = 3;
        for (int i = 0; i < numberOfUsers; i++) {
            User entry = createDbEntry(in);
            dbManager.addDbEntry(entry);
        }
        dbManager.readAndPrintDbFile();
    }

    private static String readWithRetry(Scanner scanner, String regex, String messageToShow, String errorMessage) {
        while (true) {
            System.out.println(messageToShow);
            String input = scanner.nextLine();
            if (input.matches(regex))
                return input;
            else {
                System.out.println(errorMessage);
            }
        }
    }

    private static User createDbEntry(Scanner scanner) {
        String userNameRegex = "^[a-zA-Z0-9._-]{3,}";
        String userNameErrorMessage = "Username could contain only letters, digits, characters '.', '_', '-' " +
                "and must be at least 3 characters long.";
        String username = readWithRetry(scanner, userNameRegex, "Enter username: ", userNameErrorMessage);

        String passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,256}";
        String passwordErrorMessage = "Password must contain at least 1 lowercase letter, 1 uppercase letter, " +
                "1 digit and it total must consist of 8 and up to 256 characters.";
        String password = readWithRetry(scanner, passwordRegex, "Enter password: ", passwordErrorMessage);

        String birthDateRegex = "^(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[0-2])-([12][0-9]{3})";
        String birthDateErrorMessage = "Birth Date format: DD-MM-YYYY";
        String birthDate = readWithRetry(scanner, birthDateRegex, "Enter date of birth: ", birthDateErrorMessage);

        String fullNameRegex = "^(([A-Z].*[a-z] [A-Z].*[a-z])|([А-Я].*[а-я] [А-Я].*[а-я] [А-Я].*[а-я]))";
        String fullNameErrorMessage = "Full name in english must consist of 2 words beginning with uppercase letter," +
                " in cyrillic - 3 words all beginning with uppercase letter. " +
                "Words consist of at least 2 characters and divided by spaces only.";
        String fullName = readWithRetry(scanner, fullNameRegex, "Enter full name: ", fullNameErrorMessage);

        //Did I ever tell you what the definition of insanity is?
        String emailRegex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        String emailErrorMessage = "Invalid email address. Check https://emailregex.com/";
        String email = readWithRetry(scanner, emailRegex, "Enter email: ", emailErrorMessage);

        return new User(username, password, birthDate, fullName, email);
    }
}
