package week9.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OldMain {
    public static void main(String[] args) {
//        reg1();
//        reg2();
    }

    public static void reg1() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String input;
            System.out.printf("Awaiting input: ");
            input = scanner.nextLine();
            if (input.equals("stop"))
                break;
            else
                System.out.println("input is digit? -> " + Pattern.matches("\\d+", input));
        }
    }

    public static void reg2() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String regex2 = "\\d+";

        Pattern pattern = Pattern.compile(regex2);
        Matcher matcher = pattern.matcher(input);

        int count = 0;
        while (matcher.find()) {
            count++;
            System.out.println("found: " + count + " : " + matcher.start() + " - " + matcher.end());
        }
    }
}
