package week9.regex;

import java.io.*;
import java.util.HashSet;

public class Db {

    private String DIR_PATH = System.getProperty("user.dir");

    private HashSet<User> myDatabase = new HashSet<>();

    public void addDbEntry(User entry) {
        if (!myDatabase.add(entry))
            System.out.println(entry.toString() + " are already in DB");
        else
            writeDbToFile();
    }

    private void writeDbToFile() {
        try (ObjectOutputStream out =
                     new ObjectOutputStream(
                             new FileOutputStream(
                                     new File(DIR_PATH + "\\myFictiveDB.txt")
                             ))) {

            out.writeObject(myDatabase);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readAndPrintDbFile() {
        try (ObjectInputStream in =
                     new ObjectInputStream(
                             new FileInputStream(
                                     new File(DIR_PATH + "\\myFictiveDB.txt")
                             ))) {
            HashSet<User> result = (HashSet<User>) in.readObject();
            for (User entry : result) {
                System.out.println(entry.toString());
                System.out.println();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
