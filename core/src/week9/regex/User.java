package week9.regex;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

    private String username;
    private String password;
    private String birthDate;//DD_MM_YYYY
    private String fullName;
    private String email;

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public User(String username, String password, String birthDate, String fullName, String email) {
        this.username = username;
        this.password = password;
        this.birthDate = birthDate;
        this.fullName = fullName;
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User dbEntry = (User) o;
        return username.equals(dbEntry.username) &&
                password.equals(dbEntry.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }
}
