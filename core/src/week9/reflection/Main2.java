package week9.reflection;

import java.io.File;
import java.lang.reflect.Field;

public class Main2 {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        String nameValue;
        System.out.println(myClass.toString());

        try {
            Class clazz = myClass.getClass();
            Field field = clazz.getDeclaredField("name");
            field.setAccessible(true);
            field.set(myClass, "new value");
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
