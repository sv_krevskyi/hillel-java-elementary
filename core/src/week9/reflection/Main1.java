package week9.reflection;

import week8.homework.vinylRecords.VinylRecord;

public class Main1 {
    public static void main(String[] args) {
        VinylRecord record = new VinylRecord(1996, "1","Bobby");
        Class clazz = record.getClass();
        System.out.println(clazz.getFields());
    }
}
