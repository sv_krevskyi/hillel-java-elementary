package week9.reflection;

public class MyClass {
    private int number = 15;
    private String name = "default";

    @Override
    public String toString() {
        return number + " " + name;
    }

    private void printData(){
        System.out.println(number + " " + name);
    }
}
