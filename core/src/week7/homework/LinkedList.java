package week7.homework;

public class LinkedList {
    private Node head;
    private Node tail;

    void add(int value) {
        Node toAdd = new Node(value);

        if (this.head == null) {
            head = toAdd;
            tail = toAdd;
        } else {
            tail.next = toAdd;
            tail = toAdd;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node start = this.head;
        while (start != null) {
            builder.append(start.value + " ");
            start = start.next;
        }
        return builder.toString();
    }

    class Node {
        int value;
        Node next;

        Node(int d) {
            value = d;
            next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        System.out.println(linkedList);

        System.out.println(linkedList.get(2));
    }

    public int get(int index){
        int i = 0;
        Node current = head;
        if (index == 0)
            return head.value;
        else {
            while (i != index && current != null){
                current = current.next;
                i++;
            }
            if (current != null)
                return current.value;
            else {
                throw new IndexOutOfBoundsException();
            }
        }
    }
}
