package week7.homework;

import java.util.Arrays;

public class List {

    private int[] array;
    private int index;

    List(int size) {
        this.array = new int[size];
        index = 0;
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }

    public void add(int value) {
        this.array[index++] = value;
    }

    public void add(int index, int value) {
        ensureCapacity();
        if (index <= this.index) {
            System.arraycopy(array, index, array, index + 1, array.length - index);
            array[index] = value;
            this.index++;
        } else if (index < array.length - 1) {
            array[index] = value;
            this.index++;
        } else //todo: throw an exception or resize array
            System.out.println("Index is out of bounds");
    }

    public void delete(int value) {
        ensureCapacity();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value && array[i] != 0) {
                System.arraycopy(array, i + 1, array, i, array.length - i);
                i--;
            }
        }
    }

    private void ensureCapacity() {
        if (((array.length - 1 - index) / (array.length - 1)) < 0.2) {
            int[] incrementedArray = new int[array.length * 2];
            System.arraycopy(array, 0, incrementedArray, 0, array.length);
            array = incrementedArray;
        }
    }
}
