package week7.arraycopy;

import java.util.Arrays;

public class Main2 {

    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3};

        int[] result = new int[6];

        System.arraycopy(array, 0, result, 0, array.length);

        System.out.println(Arrays.toString(result));

        String[] ar1 = new String[]{"one", "two"};
        String[] ar2 = new String[]{"three"};

        String[] res = mergeArrays(ar1, ar2);

        System.out.println(Arrays.toString(res));

        System.out.println(Arrays.toString(cutArray(new int[]{1, 2, 3, 4, 5}, 2)));

        System.out.println(Arrays.toString(addInTheMiddle(new int[]{1, 2, 3, 4}, 17, 19, 45)));

    }

    static String[] mergeArrays(String[] arr1, String[] arr2) {
        String[] result = new String[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, result, 0, arr1.length);
        System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
        return result;
    }

    static int[] cutArray(int[] arr1, int number) {
        int[] result = new int[arr1.length - number];
        System.arraycopy(arr1, 0, result, 0, result.length);
        return result;
    }

    static int[] addInTheMiddle(int[] arr, int... numbers) {
        int[] result = new int[arr.length + numbers.length];
        int mid = arr.length / 2;
        System.arraycopy(arr, 0, result, 0, mid);
        System.arraycopy(numbers, 0, result, mid, numbers.length);
        System.arraycopy(arr, mid, result, mid + numbers.length, arr.length - mid);
        return result;
    }

}
