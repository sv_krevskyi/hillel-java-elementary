package week7.exceptions;
import java.util.Arrays;
public class List {

    private int[] array;
    private int pointer;

    List (int size){
        this.array = new int[size];
        pointer = 0;
    }

    public void add(int number) throws NoAvailableSpaceException{
        System.out.println(pointer);
        if (pointer == array.length){
            throw new NoAvailableSpaceException();}

        this.array[pointer++] = number;
    }
    @Override
    public String toString() {return Arrays.toString(array);}
}
