package week7.exceptions;

public class Main {

    public static void main(String[] args) {

        week7.exceptions.List list = new List(1);

        try {
            list.add(1);
            list.add(2);
            list.add(3);
            list.add(4);

            list.add(5);

        } catch (NoAvailableSpaceException e){
            e.getStackTrace();
        }
        System.out.println(list.toString());
    }


}
