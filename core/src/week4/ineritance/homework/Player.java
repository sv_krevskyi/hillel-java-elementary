package week4.ineritance.homework;

public class Player {
    String name;
    int contractPrice;

    public Player(String name, int transferPrice){
        this.name = name;
        this.contractPrice = transferPrice;
    }

    public void play(){
        System.out.println(name + " is playing.");
    }
}
