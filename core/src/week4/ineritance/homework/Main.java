package week4.ineritance.homework;

public class Main {

    public static void main(String[] args) {
        Player[] team = new Player[11];

        team[0] = new Goalkeeper("Andriy Pyatov", 100);
        team[1] = new Defender("Oleh Husyev", 100);
        team[2] = new Defender("Taras Mykhalyk", 100);
        team[3] = new Defender("Yevhen Khacheridi", 100);
        team[4] = new Defender("Yevhen Selin", 100);
        team[5] = new Midfielder("Andriy Yarmolenko", 100);
        team[6] = new Midfielder("Anatoliy Tymoshchuk", 100);
        team[7] = new Midfielder("Yevhen Konoplyanka", 100);
        team[8] = new Midfielder("Serhiy Nazarenko", 100);
        team[9] = new Forward("Andriy Voronin", 100);
        team[10] = new Forward("Andriy Shevchenko ", 100);

        for (Player player: team) {
            if (player instanceof Goalkeeper)
                ((Goalkeeper) player).playWithHands();
            else player.play();
        }

    }
}
