package week4.ineritance.homework;

public class Goalkeeper extends Player {

    public Goalkeeper(String name, int transferPrice){
        super(name,transferPrice);
    }

    public void playWithHands(){
        System.out.println(name + " takes ball into hands.");
    }

}
