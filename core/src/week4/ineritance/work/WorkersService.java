package week4.ineritance.work;

public class WorkersService {

    public void doubleSalaryOfProgrammers(Worker[] workers) {
        for (int i = 0; i < workers.length; i++) {
            if (workers[i] instanceof Programmer)
                workers[i].salary *= 2;
        }
    }

    public void sendAstronautsToSpace(Worker[] workers){
        for (int i = 0; i < workers.length; i++) {
            if (workers[i] instanceof Astronaut)
                ((Astronaut) workers[i]).goToSpace();
        }
    }
}
