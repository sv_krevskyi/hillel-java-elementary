package week4.ineritance.work;

public class Main {

    public static void main(String[] args) {
        Programmer programmer = new Programmer();
        programmer.totalExperience = 6;
        programmer.salary = 3;
        programmer.name = "Boris The Blade";

        Designer designer = new Designer();
        designer.name = "Michael";
        designer.salary = 12;
        designer.totalExperience = 1;
        designer.creativeLvl = 23;

        Worker[] workers = new Worker[2];
        workers[0] = programmer;
        workers[1] = designer;

        for (int i = 0; i < workers.length; i++) {
            System.out.println(workers[i].salary);
            if (workers[i] instanceof Designer){
                System.out.println(((Designer) workers[i]).creativeLvl);
            }
        }
    }
}
