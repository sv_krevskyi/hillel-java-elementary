package week4.ineritance.VehicleTask.Vehicle;

import week4.ineritance.VehicleTask.Engine.Combustion;

public class Motorcycle extends Vehicle {

    public Motorcycle(int speed, int enginePower) {
        super(2, speed, 2);
        this.engine = new Combustion(enginePower);
    }
}
