package week4.ineritance.VehicleTask.Vehicle;

import week4.ineritance.VehicleTask.Engine.Engine;

public class Vehicle {
    int numberOfSeats;
    int speed;
    int numberOfWheels;
    Engine engine;

    public Vehicle(int numberOfSeats, int speed, int numberOfWheels){
        this.numberOfSeats = numberOfSeats;
        this.speed = speed;
        this.numberOfWheels = numberOfWheels;
    }
}
