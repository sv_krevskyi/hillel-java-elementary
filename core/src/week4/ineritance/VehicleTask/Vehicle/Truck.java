package week4.ineritance.VehicleTask.Vehicle;

import week4.ineritance.VehicleTask.Engine.Combustion;

public class Truck extends Vehicle {

    public Truck(int speed, int enginePower) {
        super(2, speed, 6);
        this.engine = new Combustion(enginePower);
    }
}
