package week4.ineritance.VehicleTask.Vehicle;

import week4.ineritance.VehicleTask.Engine.Electric;

public class ElectricCar extends Vehicle {

    int range;

    public ElectricCar(int speed, int enginePower) {
        super(5, speed, 4);
        this.engine = new Electric(enginePower);
    }
}
