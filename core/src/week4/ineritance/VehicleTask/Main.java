package week4.ineritance.VehicleTask;

import week4.ineritance.work.*;
import week4.ineritance.VehicleTask.Vehicle.*;

public class Main {

    public static void main(String[] args) {
        Vehicle[] vehicles = new Vehicle[3];
        vehicles[0] = new Truck(80,500);
        vehicles[1] = new Motorcycle(75,95);
        vehicles[2] = new ElectricCar(90,150);

        Worker[] workers = new Worker[3];

        workers[0].vehicle = vehicles[0];
        workers[1].vehicle = vehicles[1];
        workers[2].vehicle = vehicles[2];
    }

}
