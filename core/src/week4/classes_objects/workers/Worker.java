package week4.classes_objects.workers;

public class Worker {
    String name;
    enum positions {Manager, Programmer, Designer};
    positions position;
    int salary;

    public Worker(String name, positions position, int salary){
        this.name = name;
        this.position = position;
        this.salary = salary;
    }
}
/*Worker class; 3 fields: name, position(manager, programmer, janitor), salary(int)*/