package week4.classes_objects.workers;

public class Main {

    public static void main(String[] args) {

        Worker first = new Worker("John Doe", Worker.positions.Manager, 10_000);
        Worker second = new Worker("John Doe II", Worker.positions.Designer, 10_000);
        Worker third = new Worker("John Doe III", Worker.positions.Programmer, 10_000);
        Worker fourth = new Worker("John Doe IV", Worker.positions.Programmer, 10_000);
        //Worker[] workers = addWorkersToArray(first, second, third, fourth);
        //workersInfo(workers);

//        WorkerService richCompany = new WorkerService();
//        richCompany.doubleProgrammersSalary(workers);
//
//        workersInfo(workers);
//
//        richCompany.fireManagers(workers);
//
//        workersInfo(workers);

        Company microsoft = new Company("Microsoft", 100);
        microsoft.addWorker(first);
        microsoft.addWorker(second);
        microsoft.addWorker(third);
        microsoft.addWorker(fourth);
        workersInfo(microsoft.workers);
        microsoft.cutStuff(25);
        workersInfo(microsoft.workers);

        Company apple = new Company("Apple", 100);
        apple.addWorker2ndVer(first);
        apple.addWorker2ndVer(second);
        apple.addWorker2ndVer(third);
        apple.addWorker2ndVer(fourth);
        workersInfo(apple.workers);
        apple.cutStaff2ndVer(25);
        workersInfo(apple.workers);
    }

    static Worker[] addWorkersToArray(Worker... workers) {
        return workers;
    }

    static void workersInfo(Worker[] workers) {
        for (Worker man : workers) {
            if (man != null)
                System.out.printf("Name is %s; position - %s; salary - %d.\n", man.name, man.position, man.salary);
        }
        System.out.println();//separator
    }
}
