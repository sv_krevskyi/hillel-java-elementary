package week4.classes_objects.workers;

public class WorkerService {
    public void doubleProgrammersSalary(Worker[] workers){
        for (Worker man:workers){
            if (man.position == Worker.positions.Programmer)
                man.salary *= 2;
        }
    }

    public void fireManagers(Worker[] workers){
        for (int i = 0; i < workers.length; i++){
            if (workers[i].position == Worker.positions.Manager)
                workers[i] = null;
        }
    }

}
