package week4.classes_objects.workers;

import java.util.concurrent.ThreadLocalRandom;

public class Company {

    String title;
    int numberOfWorkers;
    Worker[] workers;
    int pointer;

    public Company(String title, int numberOfWorkers) {
        this.title = title;
        this.numberOfWorkers = numberOfWorkers;
        this.workers = new Worker[numberOfWorkers];
        this.pointer = 0;
        System.out.println(title + " is created");
    }

    public void addWorker(Worker worker) {
        for (int i = 0; i < numberOfWorkers; i++) {
            if (workers[i] == null) {
                workers[i] = worker;
                break;
            } else if (i == numberOfWorkers - 1)
                System.out.println("Number of max workers Exceeded");
        }
    }

    public void addWorker2ndVer(Worker worker) {
        if (pointer < numberOfWorkers) {
            workers[pointer] = worker;
            pointer++;
        } else
            System.out.println("Number of max workers Exceeded");
    }

    public void fireWorker(Worker worker) {
        for (int i = 0; i < numberOfWorkers; i++) {
            if (workers[i] == worker) {
                workers[i] = null;
                break;
            }
        }
    }

    public void fireWorker2ndVer(Worker worker) {
        for (int i = 0; i < numberOfWorkers; i++) {
            if (workers[i] == worker) {
                workers[i] = null;
                for (int j = i; j < numberOfWorkers - 1; j++) {
                    workers[j] = workers[j + 1];
                    if (workers[j] == null)
                        break;
                }
                pointer--;
                break;
            }
        }
    }

    public void cutStuff(int percent) {
        System.out.println("Here goes cutStaff");
        int currentlyEmployed = 0;
        int[] indexesOfCurrentlyEmployed = new int[numberOfWorkers];
        for (int i = 0; i < numberOfWorkers; i++) {
            if (workers[i] != null) {
                indexesOfCurrentlyEmployed[currentlyEmployed++] = i;
            }
        }

        int numberToFire = currentlyEmployed * percent / 100;

        for (int i = 0; i < numberToFire; ) {
            int index = ThreadLocalRandom.current().nextInt(currentlyEmployed);
            if (workers[indexesOfCurrentlyEmployed[index]] != null) {
                workers[indexesOfCurrentlyEmployed[index]] = null;
                i++;
            }
        }
    }

    public void cutStaff2ndVer(int percent) {
        System.out.println("Here goes cutStaff 2ndVer");
        int numberToFire = pointer * percent / 100;
        for (int i = numberToFire; i > 0; i--) {
            fireWorker2ndVer(workers[ThreadLocalRandom.current().nextInt(pointer)]);
        }
    }
}
