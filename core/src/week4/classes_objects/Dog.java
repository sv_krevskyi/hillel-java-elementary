package week4.classes_objects;

public class Dog {
    String breed;
    enum sizes {Small, Medium, Large};
    sizes size;
    int age;
    enum colors {White, Brown, Black};
    colors color;
    boolean isFree = true;

    public Dog(String breed, sizes size, int age, colors color){
        this.breed = breed;
        this.size = size;
        this.age = age;
        this.color = color;
    }

    public void Eat(){
        System.out.println("Eating");
    }

    public void Sleep(){
        System.out.println("Sleeping");
    }

    public void Sit(){
        System.out.println("Sitting");
    }

    public void Run(){
        System.out.println("Running");
    }
}
