package week4.classes_objects;

public class DogService {

    public void catchMongrels(Dog[] dogs) {
        for (Dog pup : dogs) {
            if (pup.breed.equalsIgnoreCase("mongrel")) {
                pup.isFree = false;
            }
        }
    }

}



/*
 * 1 - get Dog[] and sets free to false;
 * */