package week4.classes_objects;

public class Main2 {

    public static void main(String[] args) {
        Person person = new Person();
        person.age = 23;
        person.name = "Sviat";

        Dog first = new Dog("Neapolitan Mastiff", Dog.sizes.Large, 5, Dog.colors.Black);
        Dog second = new Dog("Mongrel", Dog.sizes.Small, 5, Dog.colors.White);
        Dog third = new Dog("Retriever", Dog.sizes.Medium, 5, Dog.colors.Brown);

        Dog[] dogs = addDogsToArray(first, second, third);
        dogInfo(dogs);
        DogService evilCo = new DogService();
        evilCo.catchMongrels(dogs);
        dogInfo(dogs);

    }

    static Dog[] addDogsToArray(Dog... dogs) {
        return dogs;
    }

    static void dogInfo(Dog[] dogs) {
        for (Dog pup : dogs) {
            System.out.printf("Breed is %s; size - %s; age - %d; color - %s; isFree - %b.\n", pup.breed, pup.size, pup.age, pup.color, pup.isFree);
        }
        System.out.println();//separator
    }

}
