package week10.splitSentences;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        saveStringArrayToFiles(parseSentences("Lorem Ipsum is simply dummy text of the printing and typesetting " +
                "industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown" +
                " printer took a galley of type and scrambled it to make a type specimen book. It has survived not only" +
                " five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. " +
                "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, " +
                "and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."));
    }

    public static String[] parseSentences(String input){
        return input.split("\\.");
    }

    public static void saveStringArrayToFiles(String[] input){
        for (int index = 0; index < input.length; index++) {
            String filePath = "C:\\Users\\User\\IdeaProjects\\je_krevskyi\\src\\week10\\sentence" + index + ".txt";
            try(FileOutputStream out = new FileOutputStream(new File(filePath))) {
                out.write(input[index].getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
