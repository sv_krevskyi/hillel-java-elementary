package week10.threads.splitSentencesMultiThread;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriterToFileThread extends Thread {

    private String fileName;
    private String sentence;

    public WriterToFileThread(String fileName, String sentence) {
        this.fileName = fileName;
        this.sentence = sentence;
    }

    @Override
    public void run() {
        try (Writer writer = new FileWriter(new File(fileName))) {
            System.out.println("Thread: " + Thread.currentThread().getName());
            writer.write(this.sentence);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
