package week10.threads.splitSentencesMultiThread;

public class Main {

    public static void main(String[] args) {

        String text = "Lorem Ipsum is simply dummy text of the printing and typesetting " +
                "industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown" +
                " printer took a galley of type and scrambled it to make a type specimen book. It has survived not only" +
                " five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. " +
                "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, " +
                "and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

        String[] in = parseSentences(text);
        for (int i = 0; i < in.length; i++) {
            String fileName = "C:\\Users\\User\\IdeaProjects\\je_krevskyi\\src\\week10\\threads\\splitSentencesMultiThread\\sentence" + i + ".txt";
            Thread thread = new WriterToFileThread(fileName, in[i]);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread: " + Thread.currentThread().getName() + " finished.");
    }

    public static String[] parseSentences(String input) {
        return input.split("\\.");
    }
}
