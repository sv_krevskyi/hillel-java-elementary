package week10.threads;

public class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("MyRunnable is run! Thread: " + Thread.currentThread().getName() + "\n");
    }
}
