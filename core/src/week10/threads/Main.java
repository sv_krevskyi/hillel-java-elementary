package week10.threads;

public class Main {
    public static void main(String[] args) {
        System.out.println("Thread: " + Thread.currentThread().getName() + "\n");

        Thread thread1 = new Thread(new MyRunnable());
        thread1.start();

        Thread thread = new MyThread();
        thread.start();
    }
}
