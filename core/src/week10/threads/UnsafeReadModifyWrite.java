package week10.threads;

import java.util.concurrent.atomic.AtomicInteger;

public class UnsafeReadModifyWrite {
//    private int number;
    private AtomicInteger integer = new AtomicInteger(0);

    public /*synchronized*/ void incrementNumber(){
//        number++;
        this.integer.incrementAndGet();
    }

    public int getNumber() {
        return integer.get();
    }

    public static void main(String[] args) throws InterruptedException {
        final UnsafeReadModifyWrite rmw = new UnsafeReadModifyWrite();

        for (int i = 0; i < 10_000; i++) {
            Thread thread = new Thread(() -> rmw.incrementNumber(), "T" + i);
            thread.start();
            //System.out.println(thread.getName());
        }

        Thread.sleep(5000);
        System.out.println("Final number (should be 10000): " + rmw.getNumber());
    }
}
