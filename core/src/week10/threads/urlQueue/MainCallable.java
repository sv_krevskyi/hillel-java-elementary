package week10.threads.urlQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainCallable {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        UrlsHolder urlsHolder = new UrlsHolder("https://www.baeldung.com/java-download-file",
                "https://stackoverflow.com/questions/921262/how-to-download-and-save-a-file-from-internet-using-java",
                "https://stackabuse.com/how-to-download-a-file-from-a-url-in-java/");

        String downLoadPath = "C:\\Users\\User\\IdeaProjects\\je_krevskyi\\src\\week10\\threads\\urlQueue\\";

        List<UrlDownloader> callables = new ArrayList<>();

        callables.add(new UrlDownloader(urlsHolder, downLoadPath));
        callables.add(new UrlDownloader(urlsHolder, downLoadPath));
        callables.add(new UrlDownloader(urlsHolder, downLoadPath));
        callables.add(new UrlDownloader(urlsHolder, downLoadPath));
    }
}
