package week10.threads.urlQueue;

import java.io.*;
import java.net.URL;
import java.util.concurrent.Callable;

public class UrlDownloader extends Thread implements Callable {

    private UrlsHolder holder;
    private String downloadDirectoryPath;

    public UrlDownloader(UrlsHolder urlsHolder, String downloadDirectoryPath) {
        this.holder = urlsHolder;
        this.downloadDirectoryPath = downloadDirectoryPath;
    }

    @Override
    public void run() {
        String url = holder.getNextUrl();
        if (url == null)
            return;
        String fileName = downloadDirectoryPath + "file-" + holder.incrementUrlsCount() + ".html";
        try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
        FileOutputStream out = new FileOutputStream(new File(fileName))) {
            out.write(in.readAllBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Alternative methods
    @Override
    public String call() throws Exception {
        String url = holder.getNextUrl();
        return url;
    }



}
