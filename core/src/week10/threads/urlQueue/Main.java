package week10.threads.urlQueue;

public class Main {
    public static void main(String[] args) {
        UrlsHolder urlsHolder = new UrlsHolder("https://www.baeldung.com/java-download-file",
                "https://stackoverflow.com/questions/921262/how-to-download-and-save-a-file-from-internet-using-java",
                "https://stackabuse.com/how-to-download-a-file-from-a-url-in-java/");



        String downLoadPath = "C:\\Users\\User\\IdeaProjects\\je_krevskyi\\src\\week10\\threads\\urlQueue\\";

        UrlDownloader urlDownloader1 = new UrlDownloader(urlsHolder, downLoadPath);
        UrlDownloader urlDownloader2 = new UrlDownloader(urlsHolder, downLoadPath);
        UrlDownloader urlDownloader3 = new UrlDownloader(urlsHolder, downLoadPath);
        UrlDownloader urlDownloader4 = new UrlDownloader(urlsHolder, downLoadPath);

        urlDownloader1.start();
        urlDownloader2.start();
        urlDownloader3.start();
        urlDownloader4.start();

        try {
            urlDownloader1.join();
            urlDownloader2.join();
            urlDownloader3.join();
            urlDownloader4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
