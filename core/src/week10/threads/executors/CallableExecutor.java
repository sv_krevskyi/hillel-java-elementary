package week10.threads.executors;

import java.util.concurrent.*;

public class CallableExecutor {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Callable<Integer> task = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int randomMillis = ThreadLocalRandom.current().nextInt(2000,5000);

                Thread.sleep(randomMillis);
                return  ThreadLocalRandom.current().nextInt(1000);
            }
        };

        Future<Integer> integerFuture = executorService.submit(task);
        integerFuture.get();
    }
}
