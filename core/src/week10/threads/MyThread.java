package week10.threads;

public class MyThread extends Thread {

    @Override
    public void run() {
        System.out.println("Another thread. Thread: " + Thread.currentThread().getName() + "\n");
    }
}
