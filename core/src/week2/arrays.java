package week2;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class arrays {

    public static void main(String[] args) {

        int[] testArray1 = {1, 2, 3};
        int[] testArray2 = {4, 5, 6, 7};

        isValueInArrayNTimes(testArray1, 2);
        arrayAverageValue(testArray1);
        int[] connectedArray = conectArrays(testArray1, testArray2);
        week1.arrayPrinter.printArray(connectedArray);
        multidimensionalArray();

        int[][] multiArray = new int[][] {
                {1, 3, 1, 4},
                {100, 4},
                {0, 0, 12492942},
                {1}
        };
        week1.arrayPrinter.printArray(multiArray);
        cs50MarioWithMultiArrays(5);
    }

    static void fillArrayWithRandomValues(int[] array){
        for (int item : array){
            item = ThreadLocalRandom.current().nextInt(10_000);
        }
    }

    static boolean isValueInArrayNTimes(int[] inputArray, int value){
        int counter = 0;
        for (int item:inputArray){
            if (item == value)
                counter++;
        }
        return counter == value;
    }

    static float arrayAverageValue(int[] array){
        float average;
        int sum = 0;

        for (int i = 0; i < array.length; i++){
            sum += array[i];
        }
        average = sum / array.length;
        return average;
    }

    static int[] conectArrays(int[] firstArray, int[] secondArray){
        int[] connectedArray = new int[firstArray.length + secondArray.length];
        for (int i = 0; i < firstArray.length; i++) {
            connectedArray[i] = firstArray[i];
        }
        for (int i = firstArray.length, j = 0; i < connectedArray.length; i++, j++){
            connectedArray[i] = secondArray[j];
        }
        return connectedArray;
    }

    static void  multidimensionalArray(){
        int[] firstArray = new int[5];
        int[] secondArray = new int[7];
        int[][] multiDimensionalArray = new int[2][];
        multiDimensionalArray[0] = firstArray;
        multiDimensionalArray[1] = secondArray;

        System.out.println(Arrays.toString(multiDimensionalArray[0]));
        System.out.println(Arrays.toString(multiDimensionalArray[1]));
    }

    static void initMulriArrayWithRandomValues(int[][] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = ThreadLocalRandom.current().nextInt(10_000);
            }
        }
    }

    static void cs50MarioWithMultiArrays(int height){
        String[][] multiArray = new String[height][];
        for (int i = 0; i < height; i++){
            multiArray[i] = new String[i+1];
            for (int j = 0; j < multiArray[i].length; j++) {
                multiArray[i][j] = "#";
            }
        }

        week1.arrayPrinter.printArray(multiArray);
    }

}

//Math.random() - returns random value from 0 to 1
//ThreadLocalRandom.current().nextDouble(10_000)