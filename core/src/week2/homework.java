package week2;

public class homework {

    public static void main(String[] args) {

    }

    /*Написать метод который возвращает true если в массиве нет 3 одинаковых чисел идущих подряд, false - если есть*/
    public static boolean noTriples(int[] array) {
        int counter = 0;
        boolean result = true;
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1])
                counter++;
            if (counter == 3)
                result = false;
        }
        return result;
    }

    /*Написать метод который возвращает true если все числа в массиве идут в порядке возрастания, false если нет*/
    public static boolean isIncreasing(int[] array) {
        boolean result = true;
        for (int i = 1; i < array.length; i++) {
            if (array[i] < array[i - 1])
                result = false;
        }
        return result;
    }

    /*Написать метод который считает сумму всех чисел в двумерном массиве*/
    public static int biDirectionalSum(int[][] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        return sum;
    }

    /*Написать метод, который с помощью двумерных массивов рисует в консоли пирамиду из #*/
    static void cs50MarioWithMultiArrays(int height) {
        //creating multiArray with #
        String[][] multiArray = new String[height][];
        for (int i = 0; i < height; i++) {
            multiArray[i] = new String[i + 1];
            for (int j = 0; j < multiArray[i].length; j++) {
                multiArray[i][j] = "#";
            }
        }
        //printing
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.print(multiArray[i][j]);
            }
            System.out.printf("\n");
        }
    }

}
