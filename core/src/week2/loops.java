package week2;

import week1.arrayPrinter;
import java.util.Scanner;

public class loops {

    public static void main(String[] args) {

        double input[] = requestInputs();

        printIWantToEarnMoney();
        sumFrom1to100(5, 10);
        infiniteLoop();
        breakTest();
        skipOddIterations();
        printMod3ToMaxValue(10_000);

        arrayPrinter.printArray(input);
    }

    public static double[] requestInputs() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of inputs:");
        int inputNumber = sc.nextInt();
        double input[] = new double[inputNumber];
        for (int i = 0; i < inputNumber; i++) {
            System.out.printf("Enter %d input:\n", i);
            input[i] = sc.nextDouble();
        }
        return input;
    }

    static void daysOfTheWeek(int dayOfTheWeek) {
        switch (dayOfTheWeek) {
            case 1:
                System.out.println("Its Monday!");
                break;
            case 2:
                System.out.println("Its Tuesday!");
                break;
            case 3:
                System.out.println("Its Wednesday!");
                break;
            case 4:
                System.out.println("Its Thursday!");
                break;
            case 5:
                System.out.println("Its Friday!");
                break;
            case 6:
                System.out.println("Its Saturday!");
                break;
            case 7:
                System.out.println("Its Sunday!");
                break;
            default:
                System.out.println("Wrong input data. Input should be int in range 1..7");
                break;
        }
    }

    public static void printIWantToEarnMoney() {
        for (int i = 1; i < 8; i++) {
            System.out.printf("I want to earn %d $\n", i * 500);
        }
    }

    public static void sumFrom1to100() {
        int i = 0;
        int sum = 0;
        while (i < 100) {
            sum += ++i;
        }
        System.out.println("1..100 sum is: " + sum);
    }

    public static void sumFrom1to100(int fromValue, int toValue) {
        int sum = 0;
        int value = fromValue;
        while (value <= toValue) {
            sum += value++;
        }
        System.out.printf("%d..%d sum is: %d\n", fromValue, toValue, sum);
    }

    public static void infiniteLoop() {
        for (int i = 0; true; ++i) {
            //System.out.println("Infinite loop! #" + i);
        }
    }

    public static void breakTest() {
        for (int i = 0; i < 10_000; i++) {
            if (i > 1000 && i % 15 == 0) {
                System.out.println("i = " + i);
                break;
            }
        }
    }

    public static void skipOddIterations() {
        for (int i = 0; i < 10_000; i++) {
            if (i % 2 == 1)
                continue;
            System.out.println("i = " + i);
        }
    }

    public static void printMod3ToMaxValue(int maxValue) {
        for (int i = 0; i < maxValue; i += 3) {
            System.out.println("i = " + i);
        }
    }

}

