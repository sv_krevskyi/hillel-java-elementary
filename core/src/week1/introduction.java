package week1;

import java.util.Scanner;

public class introduction {

    public static void main(String[] args) {

        int input[] = requestInputs();

        int result = comparator(input[0], input[1]);
        System.out.println("Bigger is: " + result);
        System.out.println("Sum is: " + sum(input[0], input[1]));
        System.out.println("a is between 90 and 100: " + isInRange(input[0]));
        float averageValue = mediumValue(3);
        System.out.println("Average is: " + averageValue);
        System.out.println("sumOrDoubleSumIfEqual: " + sumOrDoubleSumIfEqual(input[0], input[1]));
        System.out.println("a isModOf3or5: " + isModOf3or5(input[0]));
        System.out.println("Is There Same Digits in a and b: " + isThereSameDigits(input[0], input[1]));

    }

    public static int[] requestInputs() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of inputs:");
        int inputNumber = sc.nextInt();
        int input[] = new int[inputNumber];
        for (int i = 0; i < inputNumber; i++) {
            System.out.printf("Enter %d input:\n", i);
            input[i] = sc.nextInt();
        }
        return input;
    }

    public static int comparator(int a, int b) {
        if (a > b)
            return a;
        if (a < b)
            return b;
        else
            return 0;
    }

    public static int sum(int a, int b) {
        return a + b;
    }

    public static boolean isInRange(int value) {
        return value >= 90 && value <= 100;
    }

    public static float mediumValue(int length) {
        Scanner sc = new Scanner(System.in);
        int sum = 0;
        for (int i = 0; i < length; i++) {
            System.out.printf("Enter value %d: \n", length);
            sum += sc.nextInt();
        }
        return sum / length;
    }

    public static int sumOrDoubleSumIfEqual(int a, int b) {
        if (a == b)
            return 2 * (a + b);
        else
            return a + b;
    }

    public static boolean isModOf3or5(int val) {
        return (val % 3) == 0 || (val % 5) == 0;
    }

    public static boolean isModOf3xor5(int val) {
        return (val % 3) == 0 ^ (val % 5) == 0;
    }

    public static boolean isThereSameDigits(int a, int b) {
        boolean result = false;
        int firstDigitOfA = a / 10;
        int secondDigitOfA = a % 10;
        int firstDigitOfB = b / 10;
        int secondDigitOfB = b % 10;
        if (firstDigitOfA == firstDigitOfB || firstDigitOfA == secondDigitOfB)
            result = true;
        if (secondDigitOfA == firstDigitOfB || secondDigitOfA == secondDigitOfB)
            result = true;
        return result;
    }

}
