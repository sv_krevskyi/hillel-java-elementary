package week1;

public class homework {

    public static void main(String[] args) {

        twoAsOne(1, 2,3);
        twoAsOne2ndVer(3,2,2);
        evenlySpaced(2,6,4);
        blackJack(19,21);
        isThereSameDigits(21,10);
    }

    /*Написать метод, который принимает на вход ТРИ числа и возвращает true если возможно добавить два числа из трёх так,
     чтоб бы получилось третье. Если невозможно - false*/
    public static boolean twoAsOne(int first, int second, int third) {
        if (first == second + third)
            return true;
        else if (second == first + third)
            return true;
        else if (third == first + second)
            return true;
        else
            return false;
    }

    //Alternative solution
    public static boolean twoAsOne2ndVer(int first, int second, int third) {
        return (first == second + third || second == first + third || third == first + second);
    }

    /*Написать метод, который принимает на вход три числа и возвращает true если расстояние между первым и вторым
     и вторым и третьим - одинаково. False - если нет.*/
    public static boolean evenlySpaced(int first, int second, int third) {
        return (Math.abs(first - second) == Math.abs(second - third));
    }

    /*Написать метод, который принимает на вход два числа и возвращает то, которое ближе всего к 21, но ПРИ УСЛОВИИ
     что оно не больше 21. Если оба больше - возвращаем 0.*/
    public static int blackJack(int first, int second) {
        if (first > 21 && second > 21)
            return 0;
        else if (first > 21)
            return second;
        else if (second > 21)
            return first;
        else if (first > second)
            return first;
        else return second;
    }

    /*Написать метод, который принимает на вход два числа и возвращает true если есть цифра,
     которая находится в обоих числах.*/
    public static boolean isThereSameDigits(int a, int b){
        boolean result = false;
        int firstDigitOfA = a / 10;
        int secondDigitOfA = a % 10;
        int firstDigitOfB = b / 10;
        int secondDigitOfB = b % 10;
        if (firstDigitOfA == firstDigitOfB || firstDigitOfA == secondDigitOfB)
            result = true;
        if (secondDigitOfA == firstDigitOfB || secondDigitOfA == secondDigitOfB)
            result = true;
        return result;
    }
}
