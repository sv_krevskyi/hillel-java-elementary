package week1;

public class arrayPrinter {

    public static void printArray(double[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("Index %d: %f\n", i, array[i]);
        }
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("Index %d: %d\n", i, array[i]);
        }
    }

    public static void printArray(char[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("Index %d: %c\n", i, array[i]);
        }
    }

    public static void printArray(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("Index %d: %s\n", i, array[i]);
        }
    }


    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("Index %d: { ", i);
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.printf("}\n");
        }
    }

    public static void printArray(String[][] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("Index %d: { ", i);
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.printf("}\n");
        }
    }

}
