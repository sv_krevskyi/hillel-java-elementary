package week3;

import java.util.concurrent.ThreadLocalRandom;

public class strings {

    public static void main(String[] args) {

        randomString(25);
        printCharRange(60, 90);
        halves("Javac");
        System.out.println(stringOfEnd("blahblah",2));
        System.out.println(percentOfChars(new char []{'j', 'a'}, "jvzaa"));
        tagWrapper("<img>", "text");

    }

    public static void printCharRange(int start, int end) {
        char[] characters = new char[end - start + 1];
        for (int i = start, j = 0; i <= end; i++, j++) {
            characters[j] = (char) i;
        }
        week1.arrayPrinter.printArray(characters);
    }

    public static void randomString(int size) {
        char[] characters = new char[size];
        for (int i = 0; i < size; i++) {
            int tmp = ThreadLocalRandom.current().nextInt(Character.MAX_VALUE);
            characters[i] = (char) tmp;
        }
        week1.arrayPrinter.printArray(characters);
    }

    static String stringOfEnd(String s, int toCut) {
        String result = s.substring(s.length() - toCut);
        return result.concat(result).concat(result);
    }

    static String[] halves(String input) {
        int middle = input.length() / 2;
        String start = input.substring(0, middle);
        String end = input.substring(middle);
        String[] result = {start.toUpperCase(), end.toLowerCase()};
        System.out.printf("[ %s , %s ]\n", result[0], result[1]);
        return result;
    }

    public static int percentOfChars(char[] chars, String s) {
        int counter = 0;
        for (int i = 0; i < s.length(); i++) {
            for (char c : chars){
                if (s.charAt(i) == c ) {
                    counter++;
                }
            }
        }
        return 100 * counter/s.length();
    }

    static void tagWrapper(String tag, String input){
        String closingTag = tag.substring(0,1).concat("/").concat(tag.substring(1));
        String result = tag.concat(input).concat(closingTag);
        System.out.println(result);
    }

}