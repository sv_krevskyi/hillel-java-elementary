package week3;

public class homework {

    public static void main(String[] args) {

        int[] firstResult = alphabetNumbers("Conor McGregor");
        week1.arrayPrinter.printArray(firstResult);
        System.out.println();//separator
        System.out.println("2nd: +: 4 & 7 = " + basicOp("+", 4, 7));
        System.out.println("2nd: -: 15 & 18 = " + basicOp("-", 15, 18));
        System.out.println("2nd: *: 5 & 5 = " + basicOp("*", 5, 5));
        System.out.println("2nd: /: 49 & 7 = " + basicOp("/", 49, 7));
        System.out.println();//separator
        System.out.println("3rd: reverse \"Hello\" - " + reverseString("Hello"));
        System.out.println("3rd: reverse \"Hannah\" - " + reverseString("Hannah"));
        System.out.println("3rd: reverse \"A\" - " + reverseString("A"));
        System.out.println();//separator
        System.out.println("3rd: reverse2nd \"Hello\" - " + reverseString2ndVer("Hello"));
        System.out.println("3rd: reverse2nd \"Hannah\" - " + reverseString2ndVer("Hannah"));
        System.out.println("3rd: reverse2nd \"A\" - " + reverseString2ndVer("A"));

    }

    /*Метод, который получает на вход строку и возвращает массив интов, которое отображают порядковый номер
     каждой буквы в алфавите (пробелы или какие то символы можно заменять на -1)*/
    public static int[] alphabetNumbers(String str) {
        int[] result = new int[str.length()];
        String upperStr = str.toUpperCase();
        int unicode;
        for (int i = 0; i < result.length; i++) {
            unicode = (int) upperStr.charAt(i);
            if (unicode > 64 && unicode < 91)
                result[i] = unicode - 64;
            else
                result[i] = -1;
        }
        return result;
    }

    /*Метод, который принимает на вход строку и два числа и выполняет четыре основные математические операции.
     Метод должна возвращать результат чисел после применения выбранной операции.*/
    public static int basicOp(String s, int first, int second) {
        int result;
        int operation = s.charAt(0);
        switch (operation) {
            case 42://*
                result = first * second;
                break;
            case 43://+
                result = first + second;
                break;
            case 45://-
                result = first - second;
                break;
            case 47://"/"
                result = first / second;
                break;
            default:
                System.out.println("Wrong operation. Returning -1");
                result = -1;
                break;
        }
        return result;
    }

    /*Метод, который переворачивает строку.*/
    public static String reverseString(String s) {
        String result = new String();
        for (int i = s.length() - 1; i >= 0; i--) {
            result = result.concat(String.valueOf(s.charAt(i)));
        }
        return result;
    }

    public static String reverseString2ndVer(String s) {
        char[] result = s.toCharArray();
        int middle = s.length() / 2;
        for (int i = 0; i < middle; i++) {
            char tmp = result[i];
            result[i] = result[result.length - i - 1];
            result[result.length - i - 1] = tmp;
        }
        return String.valueOf(result);
    }

}
