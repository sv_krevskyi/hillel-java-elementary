package week6.homework;

import week1.arrayPrinter;

public class Main {

    public static void main(String[] args) {

        int[] sample = {5, 10, 11, 4, 12, 8, 6, 15};

        bubbleSort(sample);

        arrayPrinter.printArray(sample);

        System.out.println();
        System.out.println("Searched value has index: " + binarySearch(0, sample.length - 1, sample, 55));


    }

    static void bubbleSort(int[] array) {
        int ceil = array.length;
        while (ceil != 0) {
            for (int i = 1; i < ceil; i++) {
                if (array[i - 1] > array[i]) {
                    int tmp = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = tmp;
                }
            }
            ceil--;
        }
    }

    static int binarySearch(int start, int end, int[] array, int searchedValue) {
        int result;
        if (end < start)
            result = -1;
        else {
            int mid = (end - start) / 2 + start;
            if (array[mid] == searchedValue)
                result = mid;
            else if (array[mid] > searchedValue)
                result = binarySearch(start, mid - 1, array, searchedValue);
            else
                result = binarySearch(mid + 1, end, array, searchedValue);
        }
        return result;
    }

}
