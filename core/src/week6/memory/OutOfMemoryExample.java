package week6.memory;

public class OutOfMemoryExample {
    public static void main(String[] args) {
        byte[] bytes = new byte[Integer.MAX_VALUE];
        System.out.println(bytes);
    }
}
