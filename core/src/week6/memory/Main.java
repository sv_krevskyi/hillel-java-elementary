package week6.memory;

public class Main {

    public static void main(String[] args) {

        //example(5);
        int[] arr = {500, 5, 12, 1, 128, 64, 8};
        bubleSort(arr);

    }

    static void example(int iteration) {
        if (iteration == 0)
            return;
        else {
            System.out.println("Hello");
            example(iteration - 1);
        }
    }

    static void bubleSort(int[] array){
        int ceil = array.length;
        while (ceil != 0){
            for (int i = 1; i < ceil; i++){
                if (array[i - 1] > array[i]){
                    int tmp = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = tmp;
                }
            }
            ceil--;
        }

        for (int item:array) {
            System.out.println(item);
        }
    }

}
