package week8.homework.vinylRecords;

public class VinylIsInCollectionException extends Exception {
    public VinylIsInCollectionException(String message) {
        super(message);
    }
}
