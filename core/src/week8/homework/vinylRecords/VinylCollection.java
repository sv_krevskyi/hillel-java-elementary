package week8.homework.vinylRecords;

import java.util.ArrayList;
import java.util.TreeSet;

public class VinylCollection {
    //private ArrayList<VinylRecord> collection = new ArrayList<>();
    private TreeSet<VinylRecord> collection = new TreeSet<>();

    public void addToCollection(VinylRecord vinylRecord) throws VinylIsInCollectionException {
        //checkIfAlreadyInCollection(vinylRecord);
        if (!collection.add(vinylRecord))
            throw new VinylIsInCollectionException(vinylRecord + " is already in collection.");
    }

//    private void checkIfAlreadyInCollection(VinylRecord vinylRecord) throws VinylIsInCollectionException{
//        for (VinylRecord record : collection) {
//            if (record.hashCode() == vinylRecord.hashCode() && record.equals(vinylRecord))
//                throw new VinylIsInCollectionException(vinylRecord + " is already in collection.");
//        }
//    }

    public void showCollection(){
        for (VinylRecord record : collection) {
            System.out.println(record.toString());
        }
    }
}
