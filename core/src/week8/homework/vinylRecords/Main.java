package week8.homework.vinylRecords;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        VinylCollection collection = new VinylCollection();

        ArrayList<VinylRecord> records = new ArrayList<>();
        records.add(new VinylRecord(1969, "Are You Experienced", "Jimi Hendrix"));
        records.add(new VinylRecord(1980, "Back in Black", "AC/DC"));
        records.add(new VinylRecord(1987, "Appetite for Destruction ", "Guns N’ Roses"));
        records.add(new VinylRecord(1979, "The Wall ", "Pink Floyd"));
        records.add(new VinylRecord(1969, "Green River ", "Creedence Clearwater Revival"));
        records.add(new VinylRecord(1969, "Willy and the Poor Boys ", "Creedence Clearwater Revival"));
        //records.add(new VinylRecord(1969, "Are You Experienced", "Jimi Hendrix"));

        try {
            for (VinylRecord record : records) {
                collection.addToCollection(record);
            }
        } catch (VinylIsInCollectionException exception) {
            exception.printStackTrace();
        }

        collection.showCollection();
    }

}
