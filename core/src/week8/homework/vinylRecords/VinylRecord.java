package week8.homework.vinylRecords;

import java.util.Objects;

public class VinylRecord implements Comparable<VinylRecord> {
    private int year;
    private String albumName;
    private String artist;

    public VinylRecord(int year, String albumName, String artist){
        this.year = year;
        this.albumName = albumName;
        this.artist = artist;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VinylRecord record = (VinylRecord) o;
        return year == record.year &&
                Objects.equals(albumName, record.albumName) &&
                Objects.equals(artist, record.artist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, albumName, artist);
    }

    @Override
    public String toString() {
        return "VinylRecord{" +
                "year=" + year +
                ", albumName='" + albumName + '\'' +
                ", artist='" + artist + '\'' +
                '}';
    }

    @Override
    public int compareTo(VinylRecord o) {
        int compared;
        if (this.year == o.year)
            compared = 0;
        else if (this.year < o.year)
            compared = -1;
        else
            compared = 1;

        if (compared != 0)
            return compared;
        compared = this.artist.compareTo(o.artist);
        if (compared != 0)
            return compared;
        compared = this.albumName.compareTo(o.albumName);
        return compared;
    }
}
