package week8.homework.generics;

public class Pair<T, E> {

    private T firstItem;
    private E secondItem;

    public Pair(T item1, E item2) {
        this.firstItem = item1;
        this.secondItem = item2;
    }

    public String firstItemInfo(){
        return "First: ".concat(firstItem.getClass().toString());
    }

    public String secondItemInfo(){
       return "Second: ".concat(secondItem.getClass().toString());
    }

    @Override
    public String toString() {
        return firstItemInfo().concat("\n").concat(secondItemInfo());
    }
}
