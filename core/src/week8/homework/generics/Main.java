package week8.homework.generics;

public class Main {
    public static void main(String[] args) {
        Pair<Integer, Long> pair = new Pair<>(10, 100500L);

        System.out.println(pair.firstItemInfo());
        System.out.println(pair.secondItemInfo());
        System.out.println();
        System.out.println(pair.toString());
    }
}
