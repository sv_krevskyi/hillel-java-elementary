package week8.homework;

import java.util.Arrays;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        HashMap result = wordPopularity("Blah blah blah this is text without comas and this is good");
        System.out.println(result.toString());
    }

    static HashMap wordPopularity(String text) {
        HashMap<String, Integer> result = new HashMap<>();
        String[] words = text.split(" ");
        for (String word : words) {
            if (result.containsKey(word))
                result.replace(word, result.get(word) + 1);
            else
                result.put(word, 1);
        }
        return result;
    }
}
