package week8.sets;

public interface Arithmetics<T> {

    T add(T a, T b);

    T substract(T a, T b);

    T multiply(T a, T b);

    String toString(T a);
}
