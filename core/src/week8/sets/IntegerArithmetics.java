package week8.sets;

public class IntegerArithmetics implements Arithmetics<Integer> {
    @Override
    public Integer add(Integer a, Integer b) {
        return a + b;
    }

    @Override
    public Integer substract(Integer a, Integer b) {
        return a - b;
    }

    @Override
    public Integer multiply(Integer a, Integer b) {
        return a * b;
    }

    @Override
    public String toString(Integer a) {
        return a.toString();
    }
}
