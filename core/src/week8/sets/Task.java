package week8.sets;

public class Task {

    public static void main(String[] args) {
        String[] s = new String[]{"a", "b", "c", "a"};
        System.out.println(countAllOccurrences(s, "b"));
    }

    public static <T> int countAllOccurrences(T[] array, T item) {
        int counter = 0;
        for (T i : array) {
            if (i.hashCode() == item.hashCode() && i.equals(item))
                counter++;
        }
        return counter;
    }

}
