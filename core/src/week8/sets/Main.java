package week8.sets;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        TreeSet<String> stringTreeSet = new TreeSet<>();

        stringTreeSet.add("1");
        stringTreeSet.add("2");
        stringTreeSet.add("3");
        stringTreeSet.add("4");
        stringTreeSet.add("5");

        Iterator<String> iterator = stringTreeSet.iterator();

        while (iterator.hasNext()) {
            String currentItem = iterator.next();
            //System.out.println(currentItem);
        }

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < 20; i++) {
            map.put(i, 2 * i);
        }
        long i = 0;
        Iterator<Map.Entry<Integer, Integer>> it = map.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<Integer, Integer> pair = it.next();
            i += pair.getKey() + pair.getValue();
            System.out.println(pair);
        }
//        while (iterator.hasNext()){
//            String currentItem = iterator.next();
//            System.out.println(currentItem);
//        }

    }
}
