package week8.io.url;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) {
        example2();
    }

    static void example() throws FileNotFoundException{
        Scanner scanner = new Scanner(new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\url\\split.txt"));
        while (scanner.hasNext()){
            System.out.println(scanner.nextLine());
        }
    }

    static void example2(){
        try(Scanner scanner = new Scanner(new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\url\\split.txt"));){
            while (scanner.hasNextLine()){
                Scanner lineScanner = new Scanner(scanner.nextLine());
                lineScanner.useDelimiter(",");

                while (lineScanner.hasNext()){
                    String part = lineScanner.next();
                    System.out.println(Integer.valueOf(part));
                }
                System.out.println();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }
}
