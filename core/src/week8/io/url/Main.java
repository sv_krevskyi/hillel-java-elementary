package week8.io.url;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
        //read1();
        //read2();
        //read3();
        System.out.println(Arrays.toString(read4("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\url\\strings.txt")));
    }

    static void read1() throws IOException {
        FileInputStream inputStream = new FileInputStream("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\url\\text.txt");

        int data = inputStream.read();

        while (data != -1) {
            data = inputStream.read();
            System.out.println(data);
        }
    }

    static void read2() {
        try (FileInputStream inputStream = new FileInputStream("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\url\\text.txt")) {
            int availableBytes = inputStream.available();
            byte[] bytes = new byte[availableBytes];

            inputStream.read(bytes, 0, availableBytes);

            String s = new String(bytes, StandardCharsets.UTF_8);

            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void read3() throws IOException {
        File file = new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\url\\text.txt");
        Reader reader = new FileReader(file);
        BufferedReader br = new BufferedReader(reader);
        String st;
        while ((st = br.readLine()) != null)
            System.out.println(st);
    }

    static String[] read4(String filePath) {
        String[] result = null;
        try (BufferedReader br = new BufferedReader(new FileReader(new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\url\\strings.txt")));){
            //br.mark();
            result = new String[(int)br.lines().count()];
            String st;
            int i = 0;
            br.reset();
            while ((st = br.readLine()) != null){
                result[i++] = st;
                System.out.println(st);
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }



}

