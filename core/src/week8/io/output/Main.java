package week8.io.output;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        //writeToFIle(new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\text.txt"), new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\text2.txt"));
        output7();
    }

    static void output1() {
        try (OutputStream outputStream = new FileOutputStream(new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\text.txt"), true)) {
            String text = "hello";
            byte[] bytes = text.getBytes();
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void output2() {
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\text.txt"), true))) {
            String text = "hello";
            byte[] bytes = text.getBytes();
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void read2() {
        try (InputStream inputStream = new FileInputStream("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\text.txt")) {
            int availableByes = inputStream.available();
            byte[] bytes = new byte[availableByes];

            String s = new String(bytes, StandardCharsets.UTF_8);

            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void read3() {
        try (BufferedReader br = new BufferedReader(new FileReader(new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\text.txt")))) {
            String st;

            while ((st = br.readLine()) != null) {
                System.out.println(st);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    static void writeToFIle(File from, File to) {
        try (BufferedReader br = new BufferedReader(new FileReader(from));
             OutputStream output = new BufferedOutputStream(new FileOutputStream(to))) {
            String content;

            while ((content = br.readLine()) != null) {
                output.write(content.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void output5() {
        try (ObjectOutputStream output =
                     new ObjectOutputStream(
                             new FileOutputStream(
                                     new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\PersonOBJ.txt")))) {
            Person person = new Person("Sviat", 27, 175, false);
            output.writeObject(person);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void output6() {
        try (ObjectOutputStream output =
                     new ObjectOutputStream(
                             new FileOutputStream(
                                     new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\PersonOBJ.txt")))) {
            Person person = new Person("Sviat", 27, 175, false);
            output.writeObject(person);

            ObjectInputStream input =
                    new ObjectInputStream(
                            new FileInputStream(
                                    new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\PersonOBJ.txt")));

            Person person1 = (Person) input.readObject();
            System.out.println(person1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static void output7() {
        try (ObjectOutputStream output =
                     new ObjectOutputStream(
                             new FileOutputStream(
                                     new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\PersonList.txt")))) {
            ArrayList<Person> people = new ArrayList<>();
            people.add(new Person("Sviat", 27, 175, false));
            people.add(new Person("Sviat2", 16, 165, false));
            people.add(new Person("Sviat3", 18, 180, false));
            people.add(new Person("Sviat4", 42, 165, true));

            output.writeObject(people);

            ObjectInputStream input =
                    new ObjectInputStream(
                            new FileInputStream(
                                    new File("C:\\Users\\User\\IdeaProjects\\JavaElementaryKrevskyi\\src\\week8\\io\\output\\PersonList.txt")));

            ArrayList<Person> people1 = (ArrayList<Person>) input.readObject();
            for (Person p : people1) {
                System.out.println(p);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


}


