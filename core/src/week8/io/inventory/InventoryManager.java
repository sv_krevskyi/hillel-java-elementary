package week8.io.inventory;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class InventoryManager {

    public void makeInventoryFilesByPrices(String inventoryFilePath) {
        Set<Laptop> laptops = getLaptopSetFromFile(new File(inventoryFilePath));
        createFolder("C:\\Users\\user\\Desktop\\JE_krevskyi\\src\\week8\\io\\inventory\\laptops");
        File over1500laptopsFile = createFile("C:\\Users\\user\\Desktop\\JE_krevskyi\\src\\week8\\io\\inventory\\laptops\\1500+.txt");
        File under1500laptopsFile = createFile("C:\\Users\\user\\Desktop\\JE_krevskyi\\src\\week8\\io\\inventory\\laptops\\1500-.txt");
        analyzeLaptopsAndSaveToFiles(laptops, over1500laptopsFile, under1500laptopsFile);
    }

    private void analyzeLaptopsAndSaveToFiles(Set<Laptop> laptops, File over1500File, File under1500File) {
        for (Laptop laptop : laptops) {
            if (laptop.getPrice() > 1500)
                saveLaptopStringRepresentationToFile(laptop.toString(), over1500File);
            else
                saveLaptopStringRepresentationToFile(laptop.toString(), under1500File);
        }
    }

    private Set<Laptop> getLaptopSetFromFile(File file) {
        HashSet<Laptop> result = new HashSet<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                Scanner lineScanner = new Scanner((scanner.nextLine()));
                lineScanner.useDelimiter("\\|");
                while (lineScanner.hasNext()) {
                    String mark = lineScanner.next();
                    int screen = Integer.valueOf(lineScanner.next());
                    int price = Integer.valueOf(lineScanner.next());
                    Laptop laptop = new Laptop(mark, screen, price);
                    result.add(laptop);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void createFolder(String folderPath) {
        File dir = new File(folderPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
    }

    private File createFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    private void saveLaptopStringRepresentationToFile(String laptopToString, File file) {
        try(OutputStream output = new FileOutputStream(file, true)) {
            byte[] bytes = laptopToString.concat("\n").getBytes();
            output.write(bytes);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

}
