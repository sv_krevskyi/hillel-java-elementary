package week8.io.inventory;

import java.util.Objects;

public class Laptop {
    private String mark;
    private int screen;
    private int price;

    public Laptop(String mark, int screen, int price) {
        this.mark = mark;
        this.screen = screen;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laptop laptop = (Laptop) o;
        return screen == laptop.screen &&
                price == laptop.price &&
                mark.equals(laptop.mark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mark, screen, price);
    }

    @Override
    public String toString() {
        return  mark + '|' + screen + '|' + price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
