package week8.io.shoppinglist;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;

public class ShoppingList {

    HashSet<ShoppingItem> items = new HashSet<>();

    public void parseShoppingList(String path) throws IOException{
        try(Scanner scanner = new Scanner(new File(path))){
            while (scanner.hasNextLine()){
                Scanner lineScanner = new Scanner((scanner.nextLine()));
                lineScanner.useDelimiter(":");
                while (lineScanner.hasNext()){
                    ShoppingItem item = new ShoppingItem();
                    item.item = lineScanner.next();
                    item.quantity = Integer.valueOf(lineScanner.next());
                    items.add(item);
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public HashSet getItems() {
        return items;
    }

    @Override
    public String toString() {
        String result = "";
        for (ShoppingItem item:items) {
            result.concat(item.toString());
        }
        return result;
    }

}
