package week8.io.shoppinglist;

import java.util.Objects;

public class ShoppingItem {
    @Override
    public String toString() {
        return "ShoppingItem{" +
                "item='" + item + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    String item;
    int quantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingItem that = (ShoppingItem) o;
        return quantity == that.quantity &&
                Objects.equals(item, that.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, quantity);
    }
}
